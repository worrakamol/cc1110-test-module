/******************************************************************************
* File Name          : reader.c
* Author             : Pakin P.
* Version            :
* Date               : 2014/09/09
* Description        :
*******************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "reader.h"
#include <dma.h>

/* Private constants ---------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
tMain_State Main_State;
volatile tCC1110_Packet PacketProtocol,PacketTest;

volatile uint8 Receive_Flag   = 0;
volatile uint8 Transmit_Flag  = 0;
volatile uint8 CRC_ErrorFlag  = 0;
volatile uint8 startFlag = RESET;
volatile uint8 txCalibrationFlag = RESET;
volatile uint8 txUHFProtocolParameterFlag = RESET;
volatile uint8 txFrameFlag = RESET;
volatile uint8 dataRx = 0;
volatile uint8 statePacket = HEADER_SERIAL;
volatile uint8 bufferPacket[MAX_DATA_SERIAL];
volatile uint8 countData = 0;

uint8 radioPktBuffer  [PACKET_LENGTH + 3];
uint8 radioPktTransmit[PACKET_LENGTH + 3];
uint8 radioPktReceice [PACKET_LENGTH + 3];
uint8 bufferID[10][4];

uint8 MAIN_STATE  = 0x00;
uint8 mode        = 0x00;
uint8 countTx     = 0x00;
uint8 countRx     = 0x00;
uint8 countFrame  = 0x00;
uint8 countTest   = 0x00;
uint8 timeOUT     = 0x00;




/* DMA configuration descriptor used for memory copy. */
static DMA_DESC dmaConfig0, uartDmaRxTx;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/

/******************************************************************************
* @fn  radioConfigure
*
* @brief
*      This function setting all register of CC1110
*
* @param  void
*
* @return void
*
*/
void radioConfigure(void) {

 /* RF settings SoC: CC1110
  Base Frequency  : 433.919830 MHz, 426.0875MHz, 429MHz
  Channel number  : 0
  Chennel Spacing : 199.951172 kHz
  Xtal Frequency  : 26.0000  MHz
  Data Rate       : 32.7301 kBuad
  RX filter BW    : 325.000000 kHz
  Modulation      : ASK/OOK
  Deviation       : 19.042969 kHz   => Not use in ASK/OOK modulation
  TX power        : 10 dBm
  ******** Rx or Tx mode use also configuration registers ********/

  SYNC1         = 0xC7;
  SYNC0         = 0x34;
  PKTLEN        = 0xFF;   // Packet length.
  PKTCTRL1      = 0x21;   // 0x20 >>> 0x05  >> 0x00  >> 0x21
  PKTCTRL0      = 0x45;   // Packet automation control. Data whitening on.
  ADDR          = 0xB1;
  CHANNR        = 0x00;
  FSCTRL1       = 0x06; // frequency synthesizer control
  FSCTRL0       = 0x00; // frequency synthesizer control

  FREQ2         = 0x10; // frequency control word, high byte
  FREQ1         = 0xB0; // frequency control word, middle byte
  FREQ0         = 0x3F; // frequency control word, low byte
  MDMCFG4       = 0xEA; // modem configuration
  MDMCFG3       = 0x4A; // modem configuration
  MDMCFG2       = 0x33; // modem configuration
  MDMCFG1       = 0x62; // modem configuration // 0x72
  MDMCFG0       = 0xF8; // modem configuration

  DEVIATN       = 0x50; // modem deviation setting
  MCSM2         = 0x07; // main radio control state machine configuration
  MCSM1         = 0x00; // main radio control state machine configuration
  MCSM0         = 0x18; // main radio control state machine configuration

  FOCCFG        = 0x16; // frequency offset compensation configuration
  BSCFG         = 0x6D;

  //AGCCTRL2    = 0x43; // agc control
  //AGCCTRL1    = 0x43; // agc control
  //AGCCTRL0    = 0x43; // agc control

  FREND1        = 0x56;
  FREND0        = 0x11;

  FSCAL3        = 0xE9; // frequency synthesizer calibration
  FSCAL2        = 0x2A; // frequency synthesizer calibration
  FSCAL1        = 0x00; // frequency synthesizer calibration
  FSCAL0        = 0x1F; // frequency synthesizer calibration

  //TEST2       = 0x81; // various test settings
  //TEST1       = 0x35; // various test settings
  //TEST0       = 0x09; // various test settings

  PA_TABLE7     = 0x00; // pa power setting 0
  PA_TABLE6     = 0x00; // pa power setting 0
  PA_TABLE5     = 0x00; // pa power setting 0
  PA_TABLE4     = 0x00; // pa power setting 0
  PA_TABLE3     = 0x00; // pa power setting 0
  PA_TABLE2     = 0x00; // pa power setting 0
  PA_TABLE1     = 0xC0; // pa power setting 0
  PA_TABLE0     = 0x00; // pa power setting 0

  //IOCFG2      = 0x1D; // GDO2 output pin configuration.
  //IOCFG1      = 0x17; // GDO1 output pin configuration.
  //IOCFG0      = 0x16; // GDO0 output pin configuration.

}


/******************************************************************************
* @fn  init_uart
*
* @brief
*      This function use initialize pin UART
*
* @param  void
*
* @return void
*
*/
void init_uart(void) {
  /***************************************************************************
  * Setup I/O ports
  *
  * Port and pins used by USART0 operating in UART-mode are
  * RX     : P0_2
  * TX     : P0_3
  * CT/CTS : P0_4
  * RT/RTS : P0_5
  *
  * These pins can be set to function as peripheral I/O to be be used by UART0.
  * The TX pin on the transmitter must be connected to the RX pin on the receiver.
  * If enabling hardware flow control (U0UCR.FLOW = 1) the CT/CTS (Clear-To-Send)
  * on the transmitter must be connected to the RS/RTS (Ready-To-Send) pin on the
  * receiver.
  */

  // Configure USART0 for Alternative 1 => Port P0 (PERCFG.U0CFG = 0)
  // To avoid potential I/O conflict with USART1:
  // configure USART1 for Alternative 2 => Port P1 (PERCFG.U1CFG = 1)
  PERCFG = (PERCFG & ~PERCFG_U0CFG) | PERCFG_U1CFG;

  // Configure relevant Port P0 pins for peripheral function:
  // P0SEL.SELP0_2/3/4/5 = 1 => RX = P0_2, TX = P0_3, CT = P0_4, RT = P0_5
  P0SEL |= BIT5 | BIT4 | BIT3 | BIT2;

  // Initialise bitrate = 115.2 kbps (U0BAUD.BAUD_M = 34, U0GCR.BAUD_E = 12)
  U0BAUD = UART_BAUD_M;
  U0GCR = (U0GCR&~U0GCR_BAUD_E) | UART_BAUD_E;

  // Initialise UART protocol (start/stop bit, data bits, parity, etc.):

  // USART mode = UART (U0CSR.MODE = 1)
  U0CSR |= U0CSR_MODE;

  // Start bit level = low => Idle level = high  (U0UCR.START = 0)
  U0UCR &= ~U0UCR_START;

  // Stop bit level = high (U0UCR.STOP = 1)
  U0UCR |= U0UCR_STOP;

  // Number of stop bits = 1 (U0UCR.SPB = 0)
  U0UCR &= ~U0UCR_SPB;

  // Parity = disabled (U0UCR.PARITY = 0)
  U0UCR &= ~U0UCR_PARITY;

  // 9-bit data enable = 8 bits transfer (U0UCR.BIT9 = 0)
  U0UCR &= ~U0UCR_BIT9;

  // Level of bit 9 = 0 (U0UCR.D9 = 0), used when U0UCR.BIT9 = 1
  // Level of bit 9 = 1 (U0UCR.D9 = 1), used when U0UCR.BIT9 = 1
  // Parity = Even (U0UCR.D9 = 0), used when U0UCR.PARITY = 1
  // Parity = Odd (U0UCR.D9 = 1), used when U0UCR.PARITY = 1
  U0UCR &= ~U0UCR_D9;

  // Flow control = disabled (U0UCR.FLOW = 0)
  U0UCR &= ~U0UCR_FLOW;

  // Bit order = LSB first (U0GCR.ORDER = 0)
  U0GCR &= ~U0GCR_ORDER;

}

/******************************************************************************
* @fn Serial_Receive_Packets
*
* @brief
*      This function use reveive data via serial
*
* Parameters:
*
* @param  BYTE	 wait ( hence max value = 255 )
*         Data from serial uart
*
* @return void
*
*/
void Serial_Receive_Packets(uint8 data)
{
  switch(statePacket)
  {
    case HEADER_SERIAL:
      if(data == HEADER_SERIAL)
      {
        countData = 0;
        bufferPacket[countData++] = data;
        statePacket = DATA_SERIAL;
      }
      break;

    case DATA_SERIAL :
      if((data != END_SERIAL) && (countData < MAX_DATA_SERIAL))
      {
        bufferPacket[countData++] = data;
        statePacket = DATA_SERIAL;
      }
      else
      {
        bufferPacket[countData] = data;
        statePacket = HEADER_SERIAL;
        if(bufferPacket[1] == CMD_TX_CALIBRATION)
        {
          txCalibrationFlag = SET;
        }
        else if ((bufferPacket[1] == CMD_TX_UHF_PROTOCOL_PARAMETER) && (countData >= 5))
        {
          txUHFProtocolParameterFlag = SET;
        }
        else if(bufferPacket[1] == CMD_TX_FRAME)
        {
          txFrameFlag = SET;
        }
        else if(bufferPacket[1] == CMD_START_STOP)
        {
          if(startFlag == RESET)
          {
            startFlag = SET;
          }
          else
          {
            startFlag = RESET;
          }
        }
      }
      break;

    default:
      statePacket = HEADER_SERIAL;
      break;
  }
}


/******************************************************************************
* @fn  halWait
*
* @brief
*      This function waits approximately a given number of m-seconds
*      regardless of main clock speed.
*
* Parameters:
*
* @param  BYTE	 wait ( hence max value = 255 )
*         The number of m-seconds to wait.
*
* @return void
*
*/
void halWait(uint8 wait)
{
   uint32 largeWait;
   if(wait == 0)
   {return;}

	#if defined __ICC8051__

    largeWait = ((uint16) (wait << 7));
    largeWait += 59*wait;

    largeWait = (largeWait >> CLKSPD);
    while(largeWait--);

	#elif defined (SDCC) || defined (__SDCC)
	largeWait = wait * 128;
	largeWait = largeWait + (59*wait);
	largeWait = largeWait << 1;
	while(largeWait--);

	#endif

   return;
}

/******************************************************************************
* @fn  halBuiButtonPushed
*
* @brief
*      This function check button has been pushed
*
* Parameters: void
*
* @param  void
* @return void
*
*/
uint8 halBuiButtonPushed( void )
{
   uint8 i;
   uint8 value;
   static uint8 prevValue;

   if (value = BUTTON_PRESSED()){
      for(i = 0;i < BUTTON_ACTIVE_TIMEOUT; i++){
         if(!BUTTON_PRESSED()){
            value = 0;
            break;
         }
      }
   }

   if(value){
      if (!prevValue){
         value = prevValue = 1;
      }
      else{
         value = 0;
      }
   }
   else{
      prevValue = 0;
   }
   return value;
}

// sendToPC
void sendToPC( void )
{
  // Init uart0
  //init_uart();

  // Initial arm UART
  DMAARM |= DMAARM1;
//  asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");
//  asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");
//  asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");
//  asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");
//  asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");
//  asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");
  // Begin send to UART
  /* Trigger the DMA channel manually. */
  DMAREQ |= DMAREQ1;

  /* Wait for the DMA transfer to complete. */
  //while ( !(DMAIRQ & DMAIRQ_DMAIF1) );

  /* By now, the transfer is completed, so the transfer count is reached.
  * The DMA channel 1 interrupt flag is then set, so we clear it here.
  */
  //DMAIRQ &= ~DMAIRQ_DMAIF1;
}

uint16 ManEncoding (uint8 code)
{
  uint16 man = 0, testMan = 0;
  int i;
  for( i = 7; i >= 0 ; i--)
  {
    if((code >> i) & 0x01)
    {
      man |=  ((testMan+2)<<(i*2));
    }
    else
    {
      man |=  ((testMan+1)<<(i*2));
    }
    testMan = 0;
  }
  return man;
}

void dmaRadioSetup(uint8 radioMode)
{
  /* DMA Setup
     Some configuration that are common for both TX and RX:

   * CPU has priority over DMA
   * Use 8 bits for transfer count
   * No DMA interrupt when done
   * DMA triggers on radio
   * Single transfer per trigger.
   * One byte is transferred each time.
   */
  dmaConfig0.PRIORITY       = DMA_PRI_LOW;
  dmaConfig0.M8             = DMA_M8_USE_8_BITS;
  dmaConfig0.IRQMASK        = DMA_IRQMASK_DISABLE;
  dmaConfig0.TRIG           = DMA_TRIG_RADIO;
  dmaConfig0.TMODE          = DMA_TMODE_SINGLE;
  dmaConfig0.WORDSIZE       = DMA_WORDSIZE_BYTE;

  // Receiver specific DMA settings:

  // Source: RFD register
  // Destination: radioPktReceice
  // Use the first byte read + 3 (incl. 2 status bytes)
  // Sets maximum transfer count allowed (length byte + data + 2 status bytes)
  // Data source address is constant
  // Destination address is incremented by 1 byte for each write

  if(radioMode == _MODE_TX_)
  {
    // Transmitter specific DMA settings

    // Source: radioPktBuffer
    // Destination: RFD register
    // Use the first byte read + 1
    // Sets the maximum transfer count allowed (length byte + data)
    // Data source address is incremented by 1 byte
    // Destination address is constant
    dmaConfig0.SRCADDRH   = ((uint16)(radioPktTransmit) >> 8) & 0x00FF;
    dmaConfig0.SRCADDRL   = (uint16)(radioPktTransmit) & 0x00FF;
    dmaConfig0.DESTADDRH  = ((uint16)&X_RFD >> 8) & 0x00FF;
    dmaConfig0.DESTADDRL  = (uint16)&X_RFD & 0x00FF;

    dmaConfig0.VLEN       = DMA_VLEN_1_P_VALOFFIRST;

    dmaConfig0.LENH       = (((uint16)PACKET_LENGTH + 1) >> 8) & 0x00FF;
    dmaConfig0.LENL       = ((uint16)PACKET_LENGTH + 1 ) & 0x00FF;
    dmaConfig0.SRCINC     = DMA_SRCINC_1;
    dmaConfig0.DESTINC    = DMA_DESTINC_0;

  }
  else if(radioMode == _MODE_RX_)
  {
    // Receiver specific DMA settings:

    // Source: RFD register
    // Destination: radioPktReceice
    // Use the first byte read + 3 (incl. 2 status bytes)
    // Sets maximum transfer count allowed (length byte + data + 2 status bytes)
    // Data source address is constant
    // Destination address is incremented by 1 byte for each write
    dmaConfig0.SRCADDRH   = ((uint16)&X_RFD >> 8) & 0x00FF;
    dmaConfig0.SRCADDRL   = (uint16)&X_RFD & 0x00FF;
    dmaConfig0.DESTADDRH  = ((uint16)radioPktReceice >> 8) & 0x00FF;
    dmaConfig0.DESTADDRL  = (uint16)radioPktReceice & 0x00FF;

    dmaConfig0.VLEN       = DMA_VLEN_1_P_VALOFFIRST;

    dmaConfig0.LENH       = (((uint16)PACKET_LENGTH + 1) >> 8) & 0x00FF;
    dmaConfig0.LENL       = ((uint16)PACKET_LENGTH + 1) & 0x00FF;
    dmaConfig0.SRCINC     = DMA_SRCINC_0;
    dmaConfig0.DESTINC    = DMA_DESTINC_1;

  }

  // Save pointer to the DMA configuration struct into DMA-channel 0
  // configuration registers
  DMA0CFGH = ((uint16)&dmaConfig0 >> 8) & 0x00FF;
  DMA0CFGL = (uint16)&dmaConfig0 & 0x00FF;

#if defined (SDCC) || defined (__SDCC)
__asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP");
__asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP");
__asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP");
__asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP");
__asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP");
__asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP");

#elif defined __ICC8051__
  asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");
  asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");
  asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");
  asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");
  asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");
  asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");


#endif // defined

  return;
}

/******************************************************************************
* @fn  uart0StartTxDma
*
* @brief
*      Function which sets up a DMA channel for UART0 TX.
*
* @param  void
*
* @return void
*
*/
void uart0StartTxDma() {

  // Set source/destination pointer (UART TX buffer address) for UART TX DMA channel,
  // and total number of DMA word transfer (according to UART TX buffer size).
  uartDmaRxTx.SRCADDRH   = ((uint16)(radioPktReceice)>>8) & 0x00FF;
  uartDmaRxTx.SRCADDRL   = ((uint16)(radioPktReceice)) & 0x00FF;
  uartDmaRxTx.DESTADDRH  = ((uint16)(&X_U0DBUF) >> 8) & 0x00FF;
  uartDmaRxTx.DESTADDRL  = (uint16)(&X_U0DBUF) & 0x00FF;
  uartDmaRxTx.LENH       = ((PACKET_UART_LENGTH+1)>>8)&0xFF;
  uartDmaRxTx.LENL       = (PACKET_UART_LENGTH+1)&0xFF;
  uartDmaRxTx.VLEN       = DMA_VLEN_1_P_VALOFFIRST;
  // Perform 1-byte transfers
  uartDmaRxTx.WORDSIZE   = DMA_WORDSIZE_BYTE;

  // Transfer a single word after each DMA trigger
  uartDmaRxTx.TMODE      = DMA_TMODE_SINGLE;

  // DMA word trigger = USARTx TX complete
  uartDmaRxTx.TRIG       = DMA_TRIG_UTX0;

  uartDmaRxTx.SRCINC     = DMA_SRCINC_1;       // Increment source pointer by 1 word
                                               // address after each transfer.
  uartDmaRxTx.DESTINC    = DMA_DESTINC_0;      // Do not increment destination pointer:
                                               // points to USART UxDBUF register.

  uartDmaRxTx.IRQMASK    = DMA_IRQMASK_DISABLE; // Enable DMA interrupt to the CPU
  uartDmaRxTx.M8         = DMA_M8_USE_8_BITS;  // Use all 8 bits for transfer count
  uartDmaRxTx.PRIORITY   = DMA_PRI_GUARANTEED;        // DMA memory access

  // Link DMA descriptor with its corresponding DMA configuration register.
  DMA1CFGH = (uint8)((uint16)&uartDmaRxTx>>8);
  DMA1CFGL = (uint8)((uint16)&uartDmaRxTx&0x00FF);

#if defined (SDCC) || defined (__SDCC)
__asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP");
__asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP");
__asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP");
__asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP");
__asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP");
__asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP");

#elif defined __ICC8051__
  asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");
  asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");
  asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");
  asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");
  asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");
  asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");asm("NOP");


#endif // defined

}



// Control port
void controlPort() {

  // Setup output port
  P0SEL = 0x00;
  P0DIR = 0xFF;

  P1SEL &= ~(BIT4 | BIT5 | BIT6 | BIT7);
  P1DIR |= BIT4 | BIT5 | BIT6 | BIT7;

  P2SEL &= ~(BIT0);
  P2DIR |= BIT0;

  P0_7 = (radioPktReceice[1] >> 7) & 0x01;
  P0_6 = (radioPktReceice[1] >> 6) & 0x01;
  P0_5 = (radioPktReceice[1] >> 5) & 0x01;
  P0_4 = (radioPktReceice[1] >> 4) & 0x01;
  P0_3 = (radioPktReceice[1] >> 3) & 0x01;
  P0_2 = (radioPktReceice[1] >> 2) & 0x01;
  P0_1 = (radioPktReceice[1] >> 1) & 0x01;
  P0_0 = (radioPktReceice[1]) & 0x01;

  P1_7 = (radioPktReceice[2] >> 7) & 0x01;
  P1_6 = (radioPktReceice[2] >> 6) & 0x01;
  P1_5 = (radioPktReceice[2] >> 5) & 0x01;
  P1_4 = (radioPktReceice[2] >> 4) & 0x01;

  P2_0 = (radioPktReceice[2]) & 0x01;
}



void PacketTransmit(uint8 mode)
{
  uint8 txCounter = 1;
  if( mode == _TX_WAKEUP_ )
  {
	uint16 man = 0;
    /* Wakeup packet */
    txCounter = 1;
    // Premable
    for( ;txCounter <= 175; )
    {
      radioPktTransmit[txCounter++]   = 0xF0;
    }

    // sync wakeup
    man = ManEncoding(WK_SYNC);
    radioPktTransmit[txCounter++]     = (uint8)(man>>8);
    radioPktTransmit[txCounter++]     = (uint8)man;

    // wakeup key
    man = ManEncoding(WK_KEY);
    radioPktTransmit[txCounter++]     = (uint8)(man>>8);
    radioPktTransmit[txCounter++]     = (uint8)man;

    // ID[ ID3:ID0 ]
    man = ManEncoding(PacketProtocol.ID[3]);
    radioPktTransmit[txCounter++]     = (uint8)(man>>8);
    radioPktTransmit[txCounter++]     = (uint8)man;

    man = ManEncoding(PacketProtocol.ID[2]);
    radioPktTransmit[txCounter++]     = (uint8)(man>>8);
    radioPktTransmit[txCounter++]     = (uint8)man;

    man = ManEncoding(PacketProtocol.ID[1]);
    radioPktTransmit[txCounter++]     = (uint8)(man>>8);
    radioPktTransmit[txCounter++]     = (uint8)man;

    man = ManEncoding(PacketProtocol.ID[0]);
    radioPktTransmit[txCounter++]     = (uint8)(man>>8);
    radioPktTransmit[txCounter++]     = (uint8)man;

    /* Wake-up Data */
    if(PacketProtocol.BroadcastFlag == TRUE)
    {
      ADCCON1 |= 0x04;
      man = ManEncoding(((TIME_SLOT & 0x0F)<<4) | (PacketProtocol.WK_Data_CMD & 0x0F));
      radioPktTransmit[txCounter++]     = (uint8)(man>>8);
      radioPktTransmit[txCounter++]     = (uint8)man;
      ADCCON1 |= 0x04;
      PacketProtocol.WK_Number     = RNDL;
      man = ManEncoding(RNDL) ;
      radioPktTransmit[txCounter++]     = (uint8)(man>>8);
      radioPktTransmit[txCounter++]     = man;
      ADCCON1 |= 0x04;
    }
    else
    {
      ADCCON1 |= 0x04;
      man = ManEncoding(((TIME_SLOT & 0x0F)<<4) | (PacketProtocol.WK_Data_CMD & 0x0F));
      radioPktTransmit[txCounter++]     = (uint8)(man>>8);
      radioPktTransmit[txCounter++]     = (uint8)man;
      ADCCON1 |= 0x04;
      PacketProtocol.WK_Number     = RNDL;
      man = ManEncoding(RNDL) ;
      radioPktTransmit[txCounter++]     = (uint8)(man>>8);
      radioPktTransmit[txCounter++]     = (uint8)man;
      ADCCON1 |= 0x04;
    }

    radioPktTransmit[0]   = txCounter;
    // end packet wakeup tess

  }
  else if( mode == _TX_ACK_ )
  {
    txCounter = 1;

    radioPktTransmit[txCounter++]   = ADDRESS;    // Address
    radioPktTransmit[txCounter++]   = PacketProtocol.WK_Number;
    radioPktTransmit[txCounter++]   = HEADER_ACK;
    radioPktTransmit[txCounter++]   = radioPktReceice[4];
    radioPktTransmit[txCounter++]   = radioPktReceice[5];
    radioPktTransmit[txCounter++]   = radioPktReceice[6];
    radioPktTransmit[txCounter++]   = radioPktReceice[7];
    radioPktTransmit[txCounter]   = countRx;

    radioPktTransmit[0]   = txCounter;    // Length byte
  }
  else if(mode == _TX_ACK_2)
  {
    txCounter = 1;

    radioPktTransmit[txCounter++]   = 0xB1;    // Address
    radioPktTransmit[txCounter++]   = radioPktReceice[2];
    radioPktTransmit[txCounter++]   = radioPktReceice[3];
    radioPktTransmit[txCounter++]   = radioPktReceice[4];
    radioPktTransmit[txCounter]     = radioPktReceice[5];

    radioPktTransmit[0]   = txCounter;    // Length byte

  }
  else if(mode == _TX_CALIBRATION_)
  {
    txCounter = 1;

    radioPktTransmit[txCounter++] = 0xB1;
    radioPktTransmit[txCounter++] = 0xDE;
    radioPktTransmit[txCounter++] = PacketProtocol.slotMax;
    radioPktTransmit[txCounter++] = PacketProtocol.timeTagSleep;
    radioPktTransmit[txCounter]   = PacketProtocol.timeTagSniffInterval;

    radioPktTransmit[0]   = txCounter;
   }
  else if(mode == _TX_NORMAL_)
  {
    txCounter = 1;
    //radioPktTransmit[0]   = PACKET_LENGTH;    // Length byte
    radioPktTransmit[txCounter++]   = 0xD6;     // Address
    radioPktTransmit[txCounter++]   = 0x7E;     // Header
    radioPktTransmit[txCounter++]   = 0x41;     // CMD
    radioPktTransmit[txCounter++]   = 0x08;
    radioPktTransmit[txCounter++]   = 0xFF;
    radioPktTransmit[txCounter++]   = 0x80;
    radioPktTransmit[txCounter++]   = 0x00;
    radioPktTransmit[txCounter++]   = 0x10;
    radioPktTransmit[txCounter++]   = 0x12;
    radioPktTransmit[txCounter++]   = 0x5F;
    radioPktTransmit[txCounter++]   = 0x15;
    radioPktTransmit[txCounter++]   = 0x5C;
    radioPktTransmit[txCounter]     = 0xED;

    radioPktTransmit[0]   = txCounter;    // Length byte
  }
}

void clearPktReceice()
{
   uint8 j = 0;
  // Premable
  for(j = 0 ; j < PACKET_LENGTH ; j++)
  {
    radioPktReceice[j]   = 0x00;
  }
}

/*******************************************************************************
* @fn  Test_Transmit
*
* @brief
*      This function use for test transmit
*
* @param  void
*
* @return void
*
*/
void Test_Transmit(void)
{
  /* Change main state */
  //MAIN_STATE = _NOP_;

  if( countTest++ < 1 )
  {
    /* Enable CRC */
    PKTCTRL0      = 0x05;

    /* configure DMA channel for receive */
    dmaRadioSetup( _MODE_TX_ );
    PacketTransmit( _TX_NORMAL_ );
    PacketTest.mode = _TEST_TX_;

    /* abort any DMA transfer that might be in progress */
    DMAARM = 0x80 | DMAARM0;

    /* arm the dma channel for transmit */
    DMAARM |= DMAARM0 ;
    /* send strobe to enter transmit mode */
    RFST =  RFST_STX;
    //while ((MARCSTATE & MARCSTATE_MARC_STATE) != MARC_STATE_TX);
  }

}

/*******************************************************************************
* @fn  Test_Receive
*
* @brief
*      This function use for test receive
*
* @param  void
*
* @return void
*
*/
void Test_Receive(void)
{

  /* Change main state */
  //MAIN_STATE = _NOP_;
  /* with time out timeOUT*/
  timeOUT = 0;
  if( timeOUT )
  {
    MCSM2         = 0x10;
    PKTCTRL0      = 0x05;  // Enable CRC

    /* configure DMA channel for receive */
    dmaRadioSetup( _MODE_RX_ );
    //clearPktReceice();
    mode = _TEST_RX_ ;

    /* abort any DMA transfer that might be in progress */
    DMAARM = 0x80 | DMAARM0;

    /* arm the dma channel for receive */
    DMAARM |= DMAARM0 ;
    /* send strobe to enter receive mode */
    RFST = RFST_SRX;
    //while ((MARCSTATE & MARCSTATE_MARC_STATE) != MARC_STATE_RX);

    /* Reset Sleep Timer */
    WORCTRL |= 0x04;

    /* wait time out or receive packet */
    while(!(RFIF & RFIF_IRQ_TIMEOUT) && !(MAIN_STATE == _TEST_RX_));
    if( MAIN_STATE != _TEST_RX_)
    {
      //RFST = RFST_SIDLE;
      while ((MARCSTATE & MARCSTATE_MARC_STATE) != MARC_STATE_IDLE);
      //halWait(1);
      //RFST = RFST_SCAL;
      //while ((MARCSTATE & MARCSTATE_MARC_STATE) != MARC_STATE_IDLE);
      //halWait(1);

       MAIN_STATE = _TEST_RX_ ;
       RFIF &= ~RFIF_IRQ_TIMEOUT ;
       RFIF &= ~RFIF_IRQ_CS ;
    }

  }
  else
  {
    //MCSM2         = 0x07;
    //PKTCTRL0      = 0x05;  // Enable CRC
    /* configure DMA channel for receive */
    dmaRadioSetup( _MODE_RX_ );
    //clearPktReceice();
    PacketTest.mode = _TEST_RX_;

    /* abort any DMA transfer that might be in progress */
    DMAARM = 0x80 | DMAARM0;

    /* arm the dma channel for receive */
    DMAARM |= DMAARM0 ;
    /* send strobe to enter receive mode */
    RFST = RFST_SRX;
    //while ((MARCSTATE & MARCSTATE_MARC_STATE) != MARC_STATE_RX);

  }
}

/*******************************************************************************
* @fn  Test_TransmitWK
*
* @brief
*      This function use for test transmit wake up packet
*
* @param  void
*
* @return void
*
*/
void Test_TransmitWK(void)
{
  //MAIN_STATE = _NOP_ ;
  if(countTx++ < 1 )
  {
    if( countTx == 1)
    {
      MDMCFG1       = 0x22; // modem configuration // 0x72
      PKTCTRL0      = 0x01;  // Disable CRC
      /* Set DMA for Tx and mode */
      dmaRadioSetup( _MODE_TX_ );
      PacketTransmit(_TX_WAKEUP_);
      PacketTest.mode = _TEST_TXWK_;
    }

    DMAARM    = 0x80 | DMAARM0;

    DMAARM |= DMAARM0 ;

    RFST =  RFST_STX;
    //halWait(162);
    //MAIN_STATE = _TEST_WAKE_UP_;
    //while ((MARCSTATE & MARCSTATE_MARC_STATE) != MARC_STATE_TX);
  }
}

/*******************************************************************************
* @fn  Test_Transceive
*
* @brief
*      Thins function still not implement
*
* @param  void
*
* @return void
*
*/
void Test_Transceive(void)
{
  /* ? */
}

/**/
void CC1110_Protocol(void)
{
  PacketProtocol.ProtocolState = _TX_WAKEUP_;

  /* Infinite loop for protocol */
  while(INFINITE_LOOP)
  {
    //WDCTL =  0xA8;
    //WDCTL =  0x58;
    if(countFrame)
    {

    }
  }
}

/*******************************************************************************
* @fn  CC1110_Start_Application
*
* @brief
*      This function use for transmit wake-up packet broadcast to SIC8630
*      and wait receive response(ID) in time slot, keep ID to array buffer[bufferID[]]
*      and send date to PC via Serial port(UART to Serial)
*
* @param  void
*
* @return void
*
*/
void CC1110_Start_Application(tProtocol protocol,uint8 ID3, uint8 ID2, uint8 ID1, uint8 ID0)
{

  /*Parameter test Read RSSI*/
  //uint8 rssi_dec;
  //int16 rssi_dBm;
  //uint8 rssi_offset = 72;

  /**/
   if(PROTOCOL_1 == protocol)
   {
	 uint8 i,j;
     PacketProtocol.Protocol = PROTOCOL_1;
     /* Setting timer 2 */
     /* Set Timer 2 prescalar multiplier value (0x00 is interpreted as 256) */
     T2PR = 0x10;

     /* Set [T2CTL.TIP] to 64 and start Timer 2 in free running mode */
     T2CTL = (T2CTL & ~T2CTL_TIP) | T2CTL_TIP_64 ;

     /* Enter to IDEL state */
     RFST = RFST_SIDLE;
     while ((MARCSTATE & MARCSTATE_MARC_STATE) != MARC_STATE_IDLE);

     /* Check broadcast or specific  ID*/
     if(((ID3==0x00)&&(ID2==0x00)&&(ID1==0x00)&&(ID0==0x00))
        ||((ID3==0xFF)&&(ID2==0xFF)&&(ID1==0xFF)&&(ID0==0xFF)))
     {
       /* Wake-up broadcast ID[3:0] = 0x00 or 0xFF */
       PacketProtocol.ID[3] = 0x00;
       PacketProtocol.ID[2] = 0x00;
       PacketProtocol.ID[1] = 0x00;
       PacketProtocol.ID[0] = 0x00;
       PacketProtocol.BroadcastFlag = TRUE;
     }
     else
     {
       /* Wake-up specific  ID[3:0] as parameter */
       PacketProtocol.ID[3] = ID3;
       PacketProtocol.ID[2] = ID2;
       PacketProtocol.ID[1] = ID1;
       PacketProtocol.ID[0] = ID0;
       PacketProtocol.BroadcastFlag = FALSE;
     }

     PacketProtocol.countID = 0x00;
     PacketProtocol.frameTx = FRAME;
     PacketProtocol.ProtocolState = _TX_WAKEUP_;  // _TX_WAKEUP_ = 0xD0
     while(PacketProtocol.frameTx)
     {
       switch( PacketProtocol.ProtocolState )
       {
         case _TX_WAKEUP_ :
           /* Clear watchdog */
           WDCTL =  0xA8;
           WDCTL =  0x58;

           /* Change main state */
           PacketProtocol.ProtocolState = _NOP_;
           PacketProtocol.mode          = _TX_WAKEUP_;
           if(--PacketProtocol.frameTx)
           {
             //P1_0 = 1; // for debug
             /* Config some register for each state */
             //MDMCFG4       = 0x5A; // modem configuration
             //MDMCFG3       = 0x4A; // modem configuration
             MDMCFG2       = 0x30; // modem configuration // no preamble and sync word
             MDMCFG1       = 0x22; // modem configuration
             //FSCAL3        = 0xE9; // frequency synthesizer calibration
             PKTCTRL0      = 0x01; // Disable CRC

             /* abort any DMA transfer that might be in progress */
             DMAARM = 0x80 | DMAARM0;

             /* Set DMA for Tx and mode */
             dmaRadioSetup(_MODE_TX_);
             PacketTransmit(PacketProtocol.mode);

             /* arm the dma channel for transmit */
             DMAARM |= DMAARM0 ;
             RFST =  RFST_STX;
           }
           break;

         case _RX_WAIT_TIMEOUT_ :
           /* Change main state */
           PacketProtocol.ProtocolState = _NOP_;
           PacketProtocol.mode          = _RX_WAIT_TIMEOUT_;

           /* Config some register for each state */
           //MDMCFG4       = 0x5C; // modem configuration
           //MDMCFG3       = 0xF9; // modem configuration
           MDMCFG2       = 0x33; // modem configuration // sync word 30/32
           MDMCFG1       = 0x22; // modem configuration
           //FSCAL3        = 0xEA; // frequency synthesizer calibration
           PKTCTRL0      = 0x45; // Enable CRC

           /* abort any DMA transfer that might be in progress */
           DMAARM = 0x80 | DMAARM0;

           /* Set DMA for Rx and mode */
           dmaRadioSetup( _MODE_RX_ );

           /* Clear Rcceive and CRC_Error Flag */
           Receive_Flag = RESET;
           CRC_ErrorFlag = RESET;

           /* Clear Rx time out flag*/
           RFIF &= ~RFIF_IRQ_TIMEOUT ;

           /* arm the dma channel for receive */
           DMAARM |= DMAARM0 ;
           RFST = RFST_SRX;
           //P1_1 = 1; // for dubug
           /* count Rx Slot */
           if(++countRx  == 1)
           {
             halWait(1);
           }

           /* Wait Rx time slot */
           T2CT =0x00;
           T2CTL = 0x04;
           while(!(T2CTL & T2CTL_TEX));
           T2CTL = 0x00;
           T2CT =0x00;
           //P1_1 = 0; // for dubug
           if(Receive_Flag)
           {
             /* Receive packet completed */
             /* Check CRC */
             if(CRC_ErrorFlag)
             {
               /* CRC Error : Receive packet but CRC Error */
               RFST = RFST_SIDLE;
               while ((MARCSTATE & MARCSTATE_MARC_STATE) != MARC_STATE_IDLE);
               if( countRx >= TIME_SLOT )
               {
                 T2CT =0x00;
                 T2CTL = 0x04;
                 while(!(T2CTL & T2CTL_TEX));
                 T2CTL = 0x00;
                 T2CT =0x00;
                 countRx = 0;
                 PacketProtocol.ProtocolState = _TX_WAKEUP_ ;
               }
               else /* Nop */
               {
                 T2CT =0x00;
                 T2CTL = 0x04;
                 while(!(T2CTL & T2CTL_TEX));
                 T2CTL = 0x00;
                 T2CT =0x00;
                 PacketProtocol.ProtocolState = _RX_WAIT_TIMEOUT_;
               }
             }
             else  /* No CRC Error */
             {

               PacketProtocol.ProtocolState = _TX_ACK_;
             }

           }
           else
           {
             /* Rx time out */
             /* Enter to IDLE state */
             RFST = RFST_SIDLE;
             while ((MARCSTATE & MARCSTATE_MARC_STATE) != MARC_STATE_IDLE);
             if(countRx >= TIME_SLOT)
             {
               T2CT =0x00;
               T2CTL = 0x04;
               while(!(T2CTL & T2CTL_TEX));
               T2CTL = 0x00;
               T2CT =0x00;
               countRx = 0;
               PacketProtocol.ProtocolState = _TX_WAKEUP_ ;
             }
             else /* Nop */
             {
               T2CT =0x00;
               T2CTL = 0x04;
               while(!(T2CTL & T2CTL_TEX));
               T2CTL = 0x00;
               T2CT =0x00;
               PacketProtocol.ProtocolState = _RX_WAIT_TIMEOUT_;
             }
           }

           break;

         case _TX_ACK_ :
           /* Change main state */
           PacketProtocol.ProtocolState = _NOP_;

           /* Check packet valid */
           if((radioPktReceice[0] == 0x0F)&&(radioPktReceice[2] == PacketProtocol.WK_Number)&&(radioPktReceice[3] ==  HEADER_UID))
           {
			 uint8 i;
             //P1_0 = 1; // for debug
             T2CT =0x00;
             T2CTL = 0x04;

             /* Keep ID from SIC8630 response to buffer */
             for( i = 0 ; i < 4; i++)
             {
               bufferID[PacketProtocol.countID][i] = radioPktReceice[4+i];
             }

             if(++PacketProtocol.countID >= 10)
             {
               PacketProtocol.countID = 0;
             }

             radioPktReceice[0] = 0x00;

             /* Enable CRC */
             PKTCTRL0      = 0x45;

             /* configure DMA channel for receive */
             dmaRadioSetup(_MODE_TX_);
             PacketProtocol.mode = _TX_ACK_;
             PacketTransmit(PacketProtocol.mode);

             /* abort any DMA transfer that might be in progress */
             DMAARM = 0x80 | DMAARM0;

             /* arm the dma channel for transmit */
             DMAARM |= DMAARM0 ;
             /* send strobe to enter transmit mode */
             RFST =  RFST_STX;

             /* User can add activity in this below before transmit ACK packet and packet UID valid
              * such as Toggle LED or I/O, SPI, UART, and other
              */
             /*
             if(radioPktBuffer[7] == 0xDF)
             {
               P1_0 = 1;
               halWait(100);
               P1_0 = 0;
             }

             if(radioPktBuffer[7] == 0xFB)
             {
               P1_1 = 1;
               halWait(100);
               P1_1 = 0;
             }
             radioPktBuffer[7] = 0x00;
            */

             while(!(T2CTL & T2CTL_TEX));
             T2CTL = 0x00;
             T2CT =0x00;
           }
           else /* packet UID not valid */
           {
             if( countRx >= TIME_SLOT )
             {
               T2CT =0x00;
               T2CTL = 0x04;
               while(!(T2CTL & T2CTL_TEX));
               T2CTL = 0x00;
               T2CT =0x00;
               countRx = 0;
               PacketProtocol.ProtocolState = _TX_WAKEUP_ ;
             }
             else
             {
               T2CT =0x00;
               T2CTL = 0x04;
               while(!(T2CTL & T2CTL_TEX));
               T2CTL = 0x00;
               T2CT =0x00;
               PacketProtocol.ProtocolState = _RX_WAIT_TIMEOUT_;
             }
           }
           break;

         case _NOP_ :
           /* This case use for wait before interrupt will change state */
           break;

         default :
           break;

       }
     }

     /* Load data ID to buffer for send to PC via serial COM port */
     for( i = 0; i < PacketProtocol.countID; i++)
     {
       for( j = 0; j < 4; j++)
       {
          radioPktBuffer[j+1] = bufferID[i][j];
       }

       radioPktBuffer[0] = 0x04;
       sendToPC();
       halWait(150);
     }
   }else if(PROTOCOL_2 == protocol)
  {
	uint8 i;
    PacketProtocol.Protocol = PROTOCOL_2;

    /* Enter to IDEL state */
    RFST = RFST_SIDLE;
    while ((MARCSTATE & MARCSTATE_MARC_STATE) != MARC_STATE_IDLE);

    /* Wake-up broadcast ID[3:0] = 0x00 or 0xFF */
    PacketProtocol.ID[3] = 0x00;
    PacketProtocol.ID[2] = 0x00;
    PacketProtocol.ID[1] = 0x00;
    PacketProtocol.ID[0] = 0x00;
    PacketProtocol.WK_Data_CMD = 0x00;
    PacketProtocol.BroadcastFlag = TRUE;

    PacketProtocol.countID = 0x00;
    //PacketProtocol.frameTx = 2;
    PacketProtocol.ProtocolState = _TX_WAKEUP_;

    if(PacketProtocol.slotMax <= 4)
    {
      T2PR = 0x0A;
    }
    else if(PacketProtocol.slotMax <= 16)
    {
      T2PR = 0x2B;
    }
    else if(PacketProtocol.slotMax <= 32)
    {
      T2PR = 0x55;
    }
    else if(PacketProtocol.slotMax <= 64)
    {
      T2PR = 0xA5;
    }
    else if(PacketProtocol.slotMax > 64)
    {
      T2PR = 0xFE;
    }
    /* Set [T2CTL.TIP] to 1024 and start Timer 2 in free running mode */
    T2CTL = (T2CTL & ~T2CTL_TIP) | T2CTL_TIP_1024 ;

    /* For debug */
    for( i = 0 ; i < 90; i++)
    {
      PacketProtocol.buffer[i] = 0;
      radioPktBuffer[i] = 0;
    }

    while(PacketProtocol.frameTx)
    {
      switch( PacketProtocol.ProtocolState )
      {
      case _TX_WAKEUP_ :
         /* Clear watchdog */
          WDCTL =  0xA8;
          WDCTL =  0x58;

          /* Change main state */
          PacketProtocol.ProtocolState = _NOP_;
          PacketProtocol.mode          = _TX_WAKEUP_;
          if(--PacketProtocol.frameTx)
          {

            //Wake up 433.92 MHz
            FREQ1         = 0xB0; // frequency control word, middle byte
            FREQ0         = 0x3F; // frequency control word, low byte
            // Data rate 32.7301 kBaud
            MDMCFG4       = 0xEA; // modem configuration
            MDMCFG3       = 0x4A; // modem configuration
            //P1_0 = 1; // for debug
            /* Config some register for each state */
            //MDMCFG4       = 0x5A; // modem configuration
            //MDMCFG3       = 0x4A; // modem configuration
            MDMCFG2       = 0x30; // modem configuration // no preamble and sync word
            //MDMCFG1       = 0x22; // modem configuration
            //FSCAL3        = 0xE9; // frequency synthesizer calibration
            PKTCTRL0      = 0x01; // Disable CRC

            /* Set DMA for Tx and mode */
            dmaRadioSetup(_MODE_TX_);
            /* abort any DMA transfer that might be in progress */
            DMAARM = 0x80 | DMAARM0;

            PacketTransmit(PacketProtocol.mode);

            /* for debug */
            P1_1 = 1;

            /* arm the dma channel for transmit */
            DMAARM |= DMAARM0 ;
            RFST =  RFST_STX;
          }
        break;

        case _RX_BEACON_ :
          /* Change main state */
          PacketProtocol.ProtocolState = _NOP_;
          PacketProtocol.mode          = _RX_BEACON_;
          //Beacon 433.92 MHz
          FREQ1         = 0xB0; // frequency control word, middle byte
          FREQ0         = 0x3F; // frequency control word, low byte
          // Data rate 16.0675 kBaud
          MDMCFG4       = 0xE9; // modem configuration
          MDMCFG3       = 0x44; // modem configuration
          /* Config some register for each state */
          //MDMCFG4       = 0x5C; // modem configuration
          //MDMCFG3       = 0xF9; // modem configuration
          MDMCFG2       = 0x32; // modem configuration // 16/16 sync word bits detected
          MDMCFG1       = 0x22; // modem configuration // set 4 bytes preamble
          //FSCAL3        = 0xEA; // frequency synthesizer calibration
          PKTCTRL0      = 0x45; // Enable CRC

          /* abort any DMA transfer that might be in progress */
          DMAARM = 0x80 | DMAARM0;

          /* Set DMA for Rx and mode */
          dmaRadioSetup( _MODE_RX_ );

          /* Clear Rcceive and CRC_Error Flag */
          Receive_Flag = RESET;
          CRC_ErrorFlag = SET;

          /* arm the dma channel for receive */
          DMAARM |= DMAARM0 ;
          RFST = RFST_SRX;
          //P1_1 = 1;

           /* Clear watchdog */
          WDCTL =  0xA8;
          WDCTL =  0x58;

          /* Wait Rx time slot */
          T2CT =0x00;
          T2CTL = 0x07;
          while(!(T2CTL & T2CTL_TEX))
          {
            if(Receive_Flag)
            {
              Receive_Flag = RESET;
              if((CRC_ErrorFlag == RESET)&&(radioPktReceice[0]==0x05))
              {
                P1_0 = 1;

                /* CRC not error, send data to serial */
                CRC_ErrorFlag = SET;


                /* chang frequency to 433.92 MHz
                   Enter to Tx state for transmit ACK
                */
               //Set frequency 433.92 MHz
                FREQ1         = 0xB0; // frequency control word, middle byte
                FREQ0         = 0x3F; // frequency control word, low byte
                Transmit_Flag = RESET;
                /* Set DMA for Tx and mode */
                dmaRadioSetup(_MODE_TX_);
                /* abort any DMA transfer that might be in progress */
                DMAARM = 0x80 | DMAARM0;

                PacketProtocol.mode = _TX_ACK_2;
                PacketTransmit(PacketProtocol.mode);

                //halWait(1);

                /* arm the dma channel for transmit */
                DMAARM |= DMAARM0 ;
                RFST =  RFST_STX;
                //P1_1 = 1;
                while(!Transmit_Flag){};
                //P1_1 = 0;
                /*rssi_dec = RSSI;
                if (rssi_dec >= 128)
                {
                  rssi_dBm = (int16)((int16)( rssi_dec - 256) / 2) - rssi_offset;
                }
                else
                {
                  rssi_dBm = (rssi_dec / 2) - rssi_offset;
                }
                rssi_dBm = (rssi_dBm^0xFFFF) + 1;
                radioPktReceice[0]  = radioPktReceice[0] + 2;
                radioPktReceice[6]  = (uint8)((rssi_dBm&0xFF00)>>8); //store rssi_upper byte
                radioPktReceice[7]  = (uint8)(rssi_dBm&0x00FF); // store rssi_lower byte*/

                ////////////////////////////////////////////////////////////////

                sendToPC();

                ////////////////////////////////////////////////////////////////

                //Set frequency 433.92 MHz
                FREQ1         = 0xB0; // frequency control word, middle byte
                FREQ0         = 0x3F; // frequency control word, low byte
                /* abort any DMA transfer that might be in progress */
                DMAARM = 0x80 | DMAARM0;
                /* Set DMA for Rx and mode */
                dmaRadioSetup( _MODE_RX_ );

                PacketProtocol.mode = _RX_BEACON_;

                /* arm the dma channel for receive */
                DMAARM |= DMAARM0 ;
                RFST = RFST_SRX;

                P1_0 = 0;
                radioPktReceice[0] = 0x00;
                PacketProtocol.buffer[radioPktReceice[2]] += 1;
                radioPktBuffer[++PacketProtocol.countID] = radioPktReceice[2];
                //PacketProtocol.buffer[PacketProtocol.frameTx+4] += 1;
              }
              else
              {
                /* CRC Error */
                /* abort any DMA transfer that might be in progress */
                DMAARM = 0x80 | DMAARM0;
                /* Set DMA for Rx and mode */
                dmaRadioSetup( _MODE_RX_ );

                /* arm the dma channel for receive */
                DMAARM |= DMAARM0 ;
                RFST = RFST_SRX;

                //PacketProtocol.buffer[PacketProtocol.frameTx] += 1;
              }
            }

            /* Clear watchdog */
            WDCTL =  0xA8;
            WDCTL =  0x58;
          }
          T2CTL = 0x00;
          T2CT =0x00;
          //P1_0 = 0;
          //P1_1 = 0;
          PacketProtocol.ProtocolState          = _TX_WAKEUP_;
          /* Enter to IDEL state */
          RFST = RFST_SIDLE;
        break;

        case _NOP_ :

        break;

      default :

        break;
      }
    }
  }
  else if(PROTOCOL_3 == protocol)
  {
    PacketProtocol.Protocol = PROTOCOL_3;

    /* Enter to IDEL state */
    RFST = RFST_SIDLE;
    while ((MARCSTATE & MARCSTATE_MARC_STATE) != MARC_STATE_IDLE);

    /* Wake-up broadcast ID[3:0] = 0x00 or 0xFF */
    PacketProtocol.ID[3] = 0x00;
    PacketProtocol.ID[2] = 0x00;
    PacketProtocol.ID[1] = 0x00;
    PacketProtocol.ID[0] = 0x00;
    PacketProtocol.BroadcastFlag = TRUE;
    PacketProtocol.countID = 0x00;
    PacketProtocol.frameTx = 2;
    PacketProtocol.ProtocolState = _TX_WAKEUP_;

    /* Clear watchdog */
    WDCTL =  0xA8;
    WDCTL =  0x58;

     /* Change main state */
    PacketProtocol.ProtocolState = _NOP_;
    PacketProtocol.mode          = _TX_WAKEUP_;

    //Wake up 433.92 MHz
    FREQ1         = 0xB0; // frequency control word, middle byte
    FREQ0         = 0x3F; // frequency control word, low byte
    // Data rate 32.7301 kBaud
    MDMCFG4       = 0xEA; // modem configuration
    MDMCFG3       = 0x4A; // modem configuration
    //P1_0 = 1; // for debug
    /* Config some register for each state */
    //MDMCFG4       = 0x5A; // modem configuration
    //MDMCFG3       = 0x4A; // modem configuration
    MDMCFG2       = 0x30; // modem configuration // no preamble and sync word
    //MDMCFG1       = 0x22; // modem configuration
    //FSCAL3        = 0xE9; // frequency synthesizer calibration
    PKTCTRL0      = 0x01; // Disable CRC

    /* Set DMA for Tx and mode */
    dmaRadioSetup(_MODE_TX_);
    /* abort any DMA transfer that might be in progress */
    DMAARM = 0x80 | DMAARM0;

    PacketTransmit(_TX_WAKEUP_);

    Transmit_Flag = RESET;
    P1_0 = 1;

    /* arm the dma channel for transmit */
    DMAARM |= DMAARM0;
    RFST =  RFST_STX;

    while(!Transmit_Flag);

    ////////////////////////////////////////////////////////////////////////////

    /* chang frequency to 433.92 MHz
       Enter to Tx state for transmit ACK
    */
    //Set frequency 433.92 MHz
    FREQ1         = 0xB0; // frequency control word, middle byte
    FREQ0         = 0x3F; // frequency control word, low byte
        // Data rate 16.0675 kBaud
    MDMCFG4       = 0xE9; // modem configuration
    MDMCFG3       = 0x44; // modem configuration
    /* Config some register for each state */
    //MDMCFG4       = 0x5C; // modem configuration
    //MDMCFG3       = 0xF9; // modem configuration
    MDMCFG2       = 0x32; // modem configuration // 16/16 sync word bits detected
    MDMCFG1       = 0x22; // modem configuration // set 4 bytes preamble
    //FSCAL3        = 0xEA; // frequency synthesizer calibration
    PKTCTRL0      = 0x45; // Enable CRC

    /* Set DMA for Tx and mode */
    //dmaRadioSetup(_MODE_TX_);

    halWait(125);
    /* abort any DMA transfer that might be in progress */
    DMAARM = 0x80 | DMAARM0;
    dmaRadioSetup(_MODE_TX_);

    PacketProtocol.mode = _TX_CALIBRATION_;
    PacketTransmit(PacketProtocol.mode);

    Transmit_Flag = RESET;
    /* arm the dma channel for transmit */
    DMAARM |= DMAARM0 ;
    RFST =  RFST_STX;
    while(!Transmit_Flag);

    ////////////////////////////////////////////////////////////////////////////

    halWait(250);
    /* abort any DMA transfer that might be in progress */
    DMAARM = 0x80 | DMAARM0;
    dmaRadioSetup(_MODE_TX_);

    PacketProtocol.mode = _TX_CALIBRATION_;
    PacketTransmit(PacketProtocol.mode);

    Transmit_Flag = RESET;
    /* arm the dma channel for transmit */
    DMAARM |= DMAARM0 ;
    RFST =  RFST_STX;
    while(!Transmit_Flag);

    P1_0 = 0;
  }
}

/*********************END OF FILE**********************************************/
