#include "uart-if.h"



/*****************************************************************************
* Variable is used in this file.
*/
volatile uint8 RX_Buffer[Buffer_Length] = {0};
volatile uint8 Tx_Buffer[Buffer_Length] = {0};
volatile uint8 countRX_Buffer = 0;
volatile uint8 countTX_Buffer = 0;
volatile uint8 Tx_length=0;
volatile uint8 CRC_Packet;


/******************************************************************************
* @fn  init_uart
*
* @brief This function use initialize pin UART
*
* @param  void
*
* @return void
*
*/
void init_uart(void) {
  /***************************************************************************
  * Setup I/O ports
  *
  * Port and pins used by USART0 operating in UART-mode are
  * RX     : P0_2
  * TX     : P0_3
  * CT/CTS : P0_4
  * RT/RTS : P0_5
  *
  * These pins can be set to function as peripheral I/O to be be used by UART0.
  * The TX pin on the transmitter must be connected to the RX pin on the receiver.
  * If enabling hardware flow control (U0UCR.FLOW = 1) the CT/CTS (Clear-To-Send)
  * on the transmitter must be connected to the RS/RTS (Ready-To-Send) pin on the
  * receiver.
  */

  // Configure USART0 for Alternative 1 => Port P0 (PERCFG.U0CFG = 0)
  // To avoid potential I/O conflict with USART1:
  // configure USART1 for Alternative 2 => Port P1 (PERCFG.U1CFG = 1)
  PERCFG = (PERCFG & ~PERCFG_U0CFG) | PERCFG_U1CFG;

  // Configure relevant Port P0 pins for peripheral function:
  // P0SEL.SELP0_2/3/4/5 = 1 => RX = P0_2, TX = P0_3, CT = P0_4, RT = P0_5
  P0SEL |= BIT5 | BIT4 | BIT3 | BIT2;

  // Initialise bitrate = 115.2 kbps (U0BAUD.BAUD_M = 34, U0GCR.BAUD_E = 12)
  U0BAUD = UART_BAUD_M;
  U0GCR = (U0GCR&~U0GCR_BAUD_E) | UART_BAUD_E;

  // Initialise UART protocol (start/stop bit, data bits, parity, etc.):

  // USART mode = UART (U0CSR.MODE = 1)
  U0CSR |= U0CSR_MODE;

  // Start bit level = low => Idle level = high  (U0UCR.START = 0)
  U0UCR &= ~U0UCR_START;

  // Stop bit level = high (U0UCR.STOP = 1)
  U0UCR |= U0UCR_STOP;

  // Number of stop bits = 1 (U0UCR.SPB = 0)
  U0UCR &= ~U0UCR_SPB;

  // Parity = disabled (U0UCR.PARITY = 0)
  U0UCR &= ~U0UCR_PARITY;

  // 9-bit data enable = 8 bits transfer (U0UCR.BIT9 = 0)
  U0UCR &= ~U0UCR_BIT9;

  // Level of bit 9 = 0 (U0UCR.D9 = 0), used when U0UCR.BIT9 = 1
  // Level of bit 9 = 1 (U0UCR.D9 = 1), used when U0UCR.BIT9 = 1
  // Parity = Even (U0UCR.D9 = 0), used when U0UCR.PARITY = 1
  // Parity = Odd (U0UCR.D9 = 1), used when U0UCR.PARITY = 1
  U0UCR &= ~U0UCR_D9;

  // Flow control = disabled (U0UCR.FLOW = 0)
  U0UCR &= ~U0UCR_FLOW;

  // Bit order = LSB first (U0GCR.ORDER = 0)
  U0GCR &= ~U0GCR_ORDER;

}




/******************************************************************************
* @fn  Start Listening Packages
*
* @brief This function use enable RX interrupt of UART0.
*
* @param  void
*
* @return void
*
*/

void Start_Listening_Packages(void)
{
      URX0IF = 0;  //Clear any pending UART RX Interrupt Flag
      U0CSR &= ~U0CSR_RX_BYTE; //UxCSR.RX_BYTE = 0; No byte received
      U0CSR |= U0CSR_RE; // Receiver enable
      URX0IE = 1; //Enable U0RX interrupt flag
}




/******************************************************************************
* @fn  Stop Listening Packages
*
* @brief This function use disable RX interrupt of UART0.
*
* @param  void
*
* @return void
*
*/

void Stop_Listening_Packages(void)
{
      URX0IF = 0;  //Clear any pending UART RX Interrupt Flag
      U0CSR &= ~U0CSR_RX_BYTE; //UxCSR.RX_BYTE = 0; No byte received
      U0CSR &= ~U0CSR_RE; // Receiver disable
      URX0IE = 0; //Enable U0RX interrupt flag
}



/******************************************************************************
* @fn  Transmit Packages
*
* @brief This function use for transmit data by using UART0
*
* @param  tx : Is used for indicate length of data
*
* @return void
*
*/

void Transmit_Packages(uint8 tx)
{
  Tx_length = tx;
  UTX0IF = 0;   //Clear any pending UART TX Interrupt Flag
  U0CSR &= ~U0CSR_TX_BYTE;   // UxCSR.TX_BYTE = 0 : Byte not transmitted
  U0DBUF = Tx_Buffer[countTX_Buffer++]; // Send very first UART byte
  IEN2 |= IEN2_UTX0IE;  // UART TX Interrupt (IEN2.UTXxIE = 1)
}


/******************************************************************************
* @fn Check_Receive_Packet
*
* @brief This function check receive data and copy to command buffer.
*
* @param  data : Pointer point to array which will store data from RX buffer of UART0.
*
* @return PASS if data from UART is correct.
*         USART_Err_CRC if data from UART is wrong.
*         USART_Err_Header if data's header is wrong.
*/
uint8 Check_Receive_Packages(uint8 *data)
{
    uint8 i;
    uint8 len_packet;
    uint8 checkCRC_Byte;
    uint8 tempCRC;

    Stop_Listening_Packages();

    len_packet = countRX_Buffer;
    tempCRC = RX_Buffer[countRX_Buffer-1];

    for(i=0; i<len_packet-1; i++)
        {
            *(data+i) = RX_Buffer[i];
        }

        checkCRC_Byte = CheckCRC(tempCRC, len_packet);
        if(checkCRC_Byte == tempCRC)
        {
            countTX_Buffer = countRX_Buffer;
            countRX_Buffer = 0;
            for(i=0; i<Buffer_Length; i++)
            { RX_Buffer[i]=0; }
            Start_Listening_Packages();
            return PASS;
        }
        else
        {
            countRX_Buffer = 0;
            for(i=0; i<Buffer_Length; i++)
            { RX_Buffer[i]=0; }
            Start_Listening_Packages();
            return USART_Err_CRC;
        }

}



/******************************************************************************
* @fn CheckCRC
*
* @brief
*      This function use check CRC in RX_Buffer
*
* Parameters:
*
* @param  Data from serial uart
*
* @return Correct : return Result of checkSUM
*         False :   return USART_Err_CRC
*
*/
uint8 CheckCRC(uint8 Correct_CRC, uint8 len_packet)
{
    uint8 checkSUM = 0;

    checkSUM = Cal_CRC(cmd_buffer, len_packet);

    if(checkSUM != Correct_CRC)
    {
        return USART_Err_CRC;
    }
    else
    {
        CRC_Packet = checkSUM;
        return checkSUM;
    }

}

uint8 Cal_CRC(uint8 *sum, uint8 length)
{
    uint8 checkSUM = 0;
    uint8 i;
    for(i=0 ; i<length-1; i++)
    {
        checkSUM ^= *(sum+i);
    }

    return checkSUM;
}



