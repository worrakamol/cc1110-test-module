#ifndef _UART_IF_
    #define _UART_IF_
/* Include File ------------------------------------------------------------*/
    #include <mcs51/lint.h>
    #include <mcs51/8051.h>
    #include "ioCCxx10_bitdef.h"
    #include <mcs51/cc1110.h>
    #include "CC1110_test_module.h"

/* Define about using UART -------------------------------------------------*/
    #define PASS              1
    #define FALSE             0
    #define TRUE              PASS
    #define SUCCESS           PASS
    #define Buffer_Length     252

    #define HEADER_SERIAL     0xCC
    #define USART_Err_Header  0xA1
    #define USART_Err_CRC     0xA2
    #define UART_BAUD_M       0x20
    #define UART_BAUD_E       0x0C



/* Extern variable from uart-if.c file -------------------------------------*/
extern volatile uint8 RX_Buffer[Buffer_Length];
extern volatile uint8 Tx_Buffer[Buffer_Length];
extern volatile uint8 cmd_buffer[Buffer_Length];
extern volatile uint8 countRX_Buffer;
extern volatile uint8 countTX_Buffer;
extern volatile uint8 Tx_length;
extern volatile uint8 CRC_Packet;

/* Function in uart-if.c file ----------------------------------------------*/
void init_uart(void);
void Start_Listening_Packages(void);
void Stop_Listening_Packages(void);
void Transmit_Packages(uint8 tx);

uint8 Check_Receive_Packages(uint8 *data);
uint8 CheckCRC(uint8 Correct_CRC, uint8 len_packet);
uint8 Cal_CRC(uint8 *sum, uint8 length);

#endif // _uart
