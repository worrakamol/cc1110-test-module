/******************************************************************************
* File Name          : reader.h
* Author             : Pakin P.
* Version            :
* Date               : 2014/09/09
* Description        :
*******************************************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __READER_H
#define __READER_H

#if defined (SDCC) || defined (__SDCC)
/* Include cc1110 header file ------------------------------------------------*/
    #include <mcs51/lint.h>
    #include <mcs51/8051.h>
    #include "ioCCxx10_bitdef.h"
    #include <mcs51/cc1110.h>

/* Define BIT0-7 -------------------------------------------------------------*/
    #define BIT0              0x01
    #define BIT1              0x02
    #define BIT2              0x04
    #define BIT3              0x08
    #define BIT4              0x10
    #define BIT5              0x20
    #define BIT6              0x40
    #define BIT7              0x80

 /* Type Define --------------------------------------------------------------*/
    typedef signed   char   int8;
    typedef unsigned char   uint8;

    typedef signed   short  int16;
    typedef unsigned short  uint16;

    typedef signed   long   int32;
    typedef unsigned long   uint32;


    typedef void (*ISR_FUNC_PTR)(void);
    typedef void (*VFPTR)(void);

#elif defined __ICC8051__
    #include <hal_types.h>
    #include <hal_defs.h>
    #include <hal_cc8051.h>
    #include <ioCCxx10_bitdef.h>

    #if (chip == 2510)
    #include <ioCC2510.h>
    #endif
    #if (chip == 1110)
    #include <ioCC1110.h>
    #endif
    #if (chip == 2511)
    #include <ioCC2511.h>
    #endif
    #if (chip == 1111)
    #include <ioCC1111.h>
    #endif
#endif // defined

/* Exported constants --------------------------------------------------------*/

/* Exported define -----------------------------------------------------------*/

// Baudrate = 115.2 kbps (U0BAUD.BAUD_M = 34, U0GCR.BAUD_E = 12)
#define UART_BAUD_M               34
#define UART_BAUD_E               12

// Macro for getting the clock division factor
#define CLKSPD                    (CLKCON & CLKSPD_BIT)

// bit maks used to check the clock speed
#define CLKSPD_BIT                0x03

/* Button S1 */
#define BUTTON_PUSH               P1_2
#define BUTTON_PRESSED()          (!BUTTON_PUSH)
#define INIT_BUTTON()             (P1DIR &= ~(0x04))

#define BUTTON_ACTIVE_TIMEOUT     10   // Used to implement debounce

#define PACKET_LENGTH             252          // Payload length.

#define PACKET_UART_LENGTH        255    // Payload length.

#define TRUE                      1
#define FALSE                     0

#define SET                       1
#define RESET                     0

#define INFINITE_LOOP             0x01
#define MAX_BUFFER                0xFF
#define MXX_BUFFER_ID             0x04

#define CRC_OK                    0x80

#define WK_SYNC                   0x8B

#define WK_KEY                    0x4D

#define TIME_SLOT                 0x04
#define WK_DATA_CMD               0x00

#define FRAME                     0x0D

#define ADDRESS                   0xD6
#define HEADER_UID                0xCB
#define HEADER_ACK                0x9E

//#define RADIO_MODE_TX             0xF0
//#define RADIO_MODE_TX_OLNY        0xF1
//#define RADIO_MODE_RX             0xF2
//#define RADIO_MODE_RX_OLNY        0xF3

/* Protocol state */
//#define  _NOP_                    0xE0
//#define  _RX_WAIT_TIME_OUT_       0xE1
//#define  _TX_WK_                  0xE2
//#define  _RX_                     0xE3
//#define  _TEST_WAKE_UP_           0xE4
//#define  _TEST_TX_                0xE5
//#define  _TEST_RX_                0xE6
//#define  _TX_ACK_                 0xE7
//#define  _RX_CRC_ERROR_           0xE8

#define TX_WAKEUP                 0x04
#define TX_NORMAL                 0x06

#define HEADER_SERIAL             0x7E
#define DATA_SERIAL               0x34
#define END_SERIAL                0xED

#define MAX_DATA_SERIAL                     0x0A
#define CMD_TX_CALIBRATION                  0xC0
#define CMD_TX_UHF_PROTOCOL_PARAMETER       0xC1
#define CMD_START_STOP                      0xA0
#define CMD_TX_FRAME                        0xA1

#define DEFAULT_SLOT_MAX                    0x40
#define DEFAULT_TAG_TIME_SLEEP              0x0A
#define DEFAULT_TAG_TIME_SNIFF_INTERVAL     0x01

/* Exported typedef ----------------------------------------------------------*/

/* Main state */
typedef enum
{
  _TEST_TX_            =(uint8)0x4E,
  _TEST_TXWK_,
  _TEST_RX_,
  _TEST_TRANSCEIVE_,
  _APPLICATION_
} tMain_State;

/* Application/Protocol state */
typedef enum
{
  PROTOCOL_1,
  PROTOCOL_2,
  PROTOCOL_3
} tProtocol;

/* Application/Protocol state */
typedef enum
{
  _TX_WAKEUP_          = (uint8)0xD0,
  _TX_ACK_,
  _TX_NORMAL_,
  _RX_WAIT_TIMEOUT_,
  _RX_DATA_LOGGER_,
  _RX_CRC_ERROR_,
  _NOP_,
  /**/
  _RX_BEACON_,
  _TX_ACK_2,
  _TX_CALIBRATION_
} tProtocol_State;

typedef enum
{
  _MODE_TX_            = (uint8)0x7D,
  _MODE_RX_
} tRF_Mode;

typedef struct
{
  uint8 mode;
  uint8 countTx;
  uint8 countRx;
  uint8 countID;
  uint8 timeOut;
  uint8 frameTx;
  uint8 BroadcastFlag;
  uint8 WK_Number;
  uint8 WK_Data_CMD;
  tProtocol_State ProtocolState;
  tProtocol Protocol;
  uint8 slotMax;
  uint8 timeTagSleep;
  uint8 timeTagSniffInterval;
  uint8 ID[MXX_BUFFER_ID];
  uint8 bufferRx[MAX_BUFFER];
  uint8 bufferTx[MAX_BUFFER];
  uint8 buffer  [MAX_BUFFER];

} tCC1110_Packet;

typedef struct
{
  uint8 property;
} tCC1110_Property;

/* Exported macro ------------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
extern tMain_State Main_State;
extern volatile tCC1110_Packet PacketProtocol,PacketTest;

extern volatile uint8 Receive_Flag;
extern volatile uint8 Transmit_Flag;
extern volatile uint8 CRC_ErrorFlag;
extern volatile uint8 startFlag;
extern volatile uint8 txCalibrationFlag;
extern volatile uint8 txUHFProtocolParameterFlag;
extern volatile uint8 txFrameFlag;
extern volatile uint8 dataRx;
extern volatile uint8 bufferPacket[MAX_DATA_SERIAL];

extern uint8 radioPktBuffer  [PACKET_LENGTH + 3];
extern uint8 radioPktTransmit[PACKET_LENGTH + 3];
extern uint8 radioPktReceice [PACKET_LENGTH + 3];

extern uint8 MAIN_STATE ;
extern uint8 mode        ;
extern uint8 countTx   ;
extern uint8 countRx    ;
extern uint8 countFrame;
extern uint8 countTest   ;
extern uint8 timeOUT     ;


/* Exported function prototypes ----------------------------------------------*/
void radioConfigure(void);
void init_uart(void);
uint8 halBuiButtonPushed( void );
void halWait(uint8 wait);
void sendToPC(void);
uint16 ManEncoding(uint8);

void dmaRadioSetup(uint8 radioMode);
void uart0StartTxDma(void) ;
void controlPort(void);
void PacketTransmit(uint8 mode);
void clearPktReceice(void);

/* Function test */
void Test_Transmit(void);
void Test_Receive(void);
void Test_TransmitWK(void);
void Test_Transceive(void);

void Serial_Receive_Packets(uint8);

/* Function protocol */
void CC1110_Protocol(void);
void Tx_wakeup(uint8 ID3, uint8 ID2, uint8 ID1, uint8 ID0);
void CC1110_Start_Application(tProtocol protocol,uint8 ID3, uint8 ID2, uint8 ID1, uint8 ID0);
/* ---------------------------------------------------------------------------*/

#endif
