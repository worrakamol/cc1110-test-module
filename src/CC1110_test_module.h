#ifndef _CC1110_test_
 #define _CC1110_test_

/***********************************************************************************
* INCLUDES
*/
#include <mcs51/lint.h>
#include <mcs51/8051.h>
#include "ioCCxx10_bitdef.h"
#include <mcs51/cc1110.h>

/* Define -------------------------------------------------------------------------*/
    #define BIT0              0x01
    #define BIT1              0x02
    #define BIT2              0x04
    #define BIT3              0x08
    #define BIT4              0x10
    #define BIT5              0x20
    #define BIT6              0x40
    #define BIT7              0x80

/* Define state command ------------------------------------------------------------*/
    #define CC1110_TEST_LED_MODULE                 0xAA
    #define CC1110_LED_GREEN_ON                    0x01
    #define CC1110_LED_GREEN_OFF                   0x02
    #define CC1110_LED_RED_ON                      0x03
    #define CC1110_LED_RED_OFF                     0x04
    #define CC1110_LED_ALL_TOGGLE                  0x05

    #define CC1110_READ_REGISTER					0xA0
    #define CC1110_WRITE_REGISTER					0xA1
    #define CC1110_RESET_CHIP						0xA2
    #define CC1110_ClearIRQ						0xA3

    #define CC1110_RADIO_RX_CAL					0xB0
    #define CC1110_RX_SENSITIVITY433_INIT			0xB1
    #define CC1110_RADIO_MOD_FLUSH_FIFO			0xB2
    #define CC1110_RADIO_PACKET_FORMAT				0xB3
    #define CC1110_RADIO_DATA_RATE					0xB4
    #define CC1110_RADIO_CARRIER_FREQUENCY			0xCC
    #define CC1110_RADIO_MODULATION				0xB6
    #define CC1110_RADIO_CLEARIRQ_ENABLE_RXIRQ		0xB7
    #define CC1110_CMD_CHANGESTATE					0xB8
    #define CC1110_HARD_CONFIGUERXSENSE		    0xBA
    #define CC1110_RETURNCOUNTPACK					0xBB
    #define CC1110_CLEAR_TEMP_RF_REGISTER           0xA2

/* Define Radio Configure Variables -----------------------------------------*/
    #define DEFAULT_CHANBW            0x00
    #define DEFAULT_SYNC_MODE         SYNC_WORD_32  // 16/16 sync word bits detected
    #define DEFAULT_PKTLEN            0xFF
    #define DEFAULT_SYNC1             0x4C
    #define DEFAULT_SYNC0             0xB2
    #define DEFAULT_MDMCFG1           0x22
    #define DEFAULT_PA                0x0D

    #define RADIO_MODE_TX_20                0x01
    #define RADIO_MODE_TX_127               0x02
    #define RADIO_MODE_TX_WAK               0x03
    #define RADIO_MODE_RX                   0x04

    #define RADIO_PKT_RX_SENSE_TEST         0x01
    #define RADIO_PKT_RX_SENSE_LEN          25

    #define PKT_DONE                  0x05
    #define PKT_INCOMING              0x56
    #define PKT_WAIT                  0x12

    /* radio configure */
    #define NRZ_NO_WHITENING_NO_CRC         0x03
    #define NRZ_WHITENING_CRC               0x37
    #define MAN_NO_WHITENING_NO_CRC         0x47
    #define MAN_NO_WHITENING_CRC            0x67
    #define TX_WAKEUP                       0x80

    #define DATARATE_10K                    0x93
    #define DATARATE_32K                    0x4A
    #define DATARATE_100K                   0xF8
    #define DATARATE_200K                   0xF9

    #define FREQUENCY_433_52                0x21
    #define FREQUENCY_433_72                0x22
    #define FREQUENCY_433_92                0x23
    #define FREQUENCY_434_12                0x24
    #define FREQUENCY_434_32                0x25

    #define FREQUENCY_867_95                0x31
    #define FREQUENCY_868_15                0x32
    #define FREQUENCY_868_35                0x33
    #define FREQUENCY_868_55                0x34
    #define FREQUENCY_868_75                0x35

    #define MODULATION_OOK                  0x30
    #define MODULATION_FSK_433              0x00
    #define MODULATION_FSK_868              0x00
    #define MODULATION_FSK_MASK             0x10  /* When mask == 1 mod is FSK, mask == 0 mod is OOK */

    #define TX_POWER_433_10_DBM             0xC0
    #define TX_POWER_433_7_DBM              0xC8
    #define TX_POWER_433_5_DBM              0x84
    #define TX_POWER_433_0_DBM              0x60
    #define TX_POWER_433_MINUS_1_DBM        0x62
    #define TX_POWER_433_MINUS_5_DBM        0x2C
    #define TX_POWER_433_MINUS_10_DBM       0x34
    #define TX_POWER_433_MINUS_15_DBM       0x1D
    #define TX_POWER_433_MINUS_20_DBM       0x0E
    #define TX_POWER_433_MINUS_30_DBM       0x12

    #define TX_POWER_868_10_DBM             0xC2
    #define TX_POWER_868_7_DBM              0xCB
    #define TX_POWER_868_5_DBM              0x84
    #define TX_POWER_868_0_DBM              0x50
    #define TX_POWER_868_MINUS_1_DBM        0x52
    #define TX_POWER_868_MINUS_5_DBM        0x8F
    #define TX_POWER_868_MINUS_10_DBM       0x27
    #define TX_POWER_868_MINUS_15_DBM       0x1E
    #define TX_POWER_868_MINUS_20_DBM       0x0E
    #define TX_POWER_868_MINUS_30_DBM       0x03

    #define RX_SENSE_TEST_MODE_SPLIT        0x0A
    #define RX_SENSE_TEST_MODE_STREAM       0x01

    #define SYNC_WORD_32                    0x03
    #define SYNC_WORD_16                    0x02
    #define NO_SYNC_WORD                    0x00
    /* error */
    #define RADIO_CFG_PASS                  0x01
    #define ERROR_CFG_INVALID               0x02
    #define RADIO_PKT_PASS                  0x01
    #define RADIO_PKT_LEN_FAIL              0x02
    #define RADIO_PKT_DATA_FAIL             0x03

    /* radio define bit */
    #define PKTCTRL0_WHITENING        0x40
    #define PKTCTRL0_CRC              0x04
    #define PKTCTRL0_LEN              0x01

    #define MDMCFG2_FSK               0x00
    #define MDMCFG2_OOK               0x30
    #define MDMCFG2_MAN               0x08
    #define MDMCFG2_NRZ               0x00

    #define MDMCFG3_10K               0x93
    #define MDMCFG3_32K               0x4A
    #define MDMCFG3_100K              0xF8
    #define MDMCFG3_200K              0xF8

    #define MDMCFG4_10K               0x08
    #define MDMCFG4_32K               0x0A
    #define MDMCFG4_100K              0x0B
    #define MDMCFG4_200K              0x0C

    /**************** Configure Bandwidth *************************
    * Bandwidth of signal which is modulated with ASK and FSK are
    *  different. Which bandwidth of RF filter can calculate follow these equation:
    *
    *  Bandwidth of ASK is 2 x DataRate kHz
    *  Bandwidth of FSK is (2 x DataRate) + (2 x Frequency Deviation)  kHz
    *
    *  Frequency Deviation configure in DEVIATN register, in this case
    *  the value is 0x50 that give frequency deviation is 50.78125 kHz,
    *  which is constant. So the value which are configured in MDMCFG4 register
    *  will follow as below.
    */

    #define MDMCFG4_BW_10K_OOK        0xF0
    #define MDMCFG4_BW_32K_OOK        0xE0
    #define MDMCFG4_BW_100K_OOK       0x80
    #define MDMCFG4_BW_200K_OOK       0x40

    #define MDMCFG4_BW_10K_FSK        0xB0//0xA0
    #define MDMCFG4_BW_32K_FSK        0x90
    #define MDMCFG4_BW_100K_FSK       0x50
    #define MDMCFG4_BW_200K_FSK       0x20
    /*******************************************************************/

    #define FSCAL_10K                 0xE9
    #define FSCAL_32K                 0xE9
    #define FSCAL_100K                0xEA
    #define FSCAL_200K                0xEA

    #define FREND0_FSK                0x10
    #define FREND0_OOK                0x11

/* Define RF state command --------------------------------------------------*/
    #define STATE_IDLE                0x00
    #define STATE_RX                  0x16
    #define STATE_TX                  0x1A

/* Value for RF_State variable -----------------------------------------------*/
    #define _IDLE_                    0x00
    #define _RX_                      0x01
    #define _TX_                      0x02
    #define _CHANGE_                  0xFF
    #define _RE_CNT_PKT_              0xFE

/* PKTSTATUS Define ----------------------------------------------------------*/
    #define CRC_OK                    0x80
    #define CS                        0x40
    #define PQT_REACHED               0x20
    #define CCA                       0x08
    #define SFD                       0x04

/* Define Flag RF ------------------------------------------------------------*/
    #define RESET                     0x00
    #define SET                       0x01
    #define CNT_PKT                   0x02


/* Type Define ---------------------------------------------------------------*/
    typedef signed   char   int8;
    typedef unsigned char   uint8;

    typedef signed   short  int16;
    typedef unsigned short  uint16;

    typedef signed   long   int32;
    typedef unsigned long   uint32;


    typedef void (*ISR_FUNC_PTR)(void);
    typedef void (*VFPTR)(void);

/*******************************************************************************
* Constant Variables
*/

/* Function is implemented in a CC1110_test_module.c file.*/
void halWait(uint8 wait);
void Resp_Package(uint8 *cmd, uint8 Tx_len);
uint8 Get_Protocol_S(uint8 *cmd);
void Select_cmd(uint8 *cmd);


/* Command option ------------------------------------------------------------------------*/
uint8 LED_Command(uint8 *led_cmd);
uint8 Radio_Package_Format(uint8 *PKT);
uint8 Radio_Modulation(uint8 *MOD);
uint8 Radio_Datarate(uint8 *D_RATE);
uint8 Radio_Init(void);
uint8 Radio_Carrier_Freq(uint8 *freq);
uint8 Radio_Change_State(uint8 *state);
void dmaRadioConfig(uint8 radioMODE);
uint8 Assert_cnt_Pack(uint8 *cnt_pack);

/* Radio configure RF TX/RF register------------------------------------------------------*/
void radio_setting_TX(void);
void radio_setting_RX(void);

/* Extern variable from CC1110_test_module.c file ----------------------------------------*/
extern volatile uint8 RF_State;
extern uint8 radioBuffer_RX_Test[22];



#endif // _CC1110_test_
