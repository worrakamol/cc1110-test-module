#include "uart-if.h"
#include "CC1110_test_module.h"
#include "dma.h"

/******************************************************************************
* The function is used in this file.
*/
uint8 Clear_RF_Reg(void);

/******************************************************************************
* The variable is used in this file.
*/
static DMA_DESC dmaConfig0;
volatile uint8 RF_State = _IDLE_;
extern volatile uint8 radio_Packet_RX[Buffer_Length];
extern volatile uint8 radio_Packet_TX[Buffer_Length];
extern volatile uint8 pkt_error_cnt;
extern volatile uint8 pkt_ok_cnt;
extern volatile uint8 ReceiveFlag;
extern volatile uint8 Return_Flag;

/******************************************************************************
* Global variable in this file for configure RF register.
*/
uint8 temp_mdmcfg2;
uint8 temp_mdmcfg1;
uint8 temp_pktctrl0;
uint8 temp_frend0;
uint8 temp_mdmcfg4;
uint8 temp_mdmcfg3;
uint8 temp_fscal3;
uint8 temp_freq2;
uint8 temp_freq1;
uint8 temp_freq0;


/******************************************************************************
* @fn halWait
*
* @brief This function use for delay in ms(milli seconds)
*
* @param void
*/

void halWait(uint8 wait)
{
   uint32 largeWait;
   if(wait == 0)
   {return;}

	largeWait = wait * 128;
	largeWait = largeWait + (59*wait);
	largeWait = largeWait << 1;
	while(largeWait--);

   return;
}




/******************************************************************************
* @fn Resp_Package
*
* @brief This function use return packages to PC by using UART
*        In this case use UART0 for communicate to PC
*
* @param cmd : Unsigned char (uint8) pointer point to any array which
*              contain data to send.
*        Tx_len : Length of data to send.
*
*/

void Resp_Package(uint8 *cmd, uint8 Tx_len)
{
    uint8 i;
    for(i=0; i<Tx_len; i++)
    {
        Tx_Buffer[i] = *(cmd+i);
    }
    countTX_Buffer = 0;
    Transmit_Packages(Tx_len);
}


/******************************************************************************
* @fn Get_Protocol_S
*
* @brief This function use for receive and check CRC of data (command) from PC.
*
* @param cmd: unsigned char pointer point to array for store data (command) from RX UART0
*
* @return PASS : Receive correct package.
*         FALSE : Receive wrong package(package error).
*/
uint8 Get_Protocol_S(uint8 *cmd)
{

        if((RX_Buffer[0]== 0xCC)&&(RX_Buffer[1]==countRX_Buffer)) //Check header and Receive completed.
        {
                if(Check_Receive_Packages(cmd) != PASS)
                {
                    return FALSE;
                }
                else
                {
                    return PASS;
                }
        }
        else
        {
            return FALSE;
        }

}


/******************************************************************************
* @fn Select_cmd (Select Command)
*
* @brief This function will check and execute command. Then return package which
*        combine flag success or false to PC.
*
* @param cmd: Unsigned char pointer point to array which store command.
*
*/

void Select_cmd(uint8 *cmd)
{
    uint8 flag;
    switch(*(cmd+2))
    {
        case CC1110_TEST_LED_MODULE: flag = LED_Command(cmd);
            break;
        case CC1110_RX_SENSITIVITY433_INIT: flag = Radio_Init();
            break;
        case CC1110_RADIO_PACKET_FORMAT: flag = Radio_Package_Format(cmd);
            break;
        case CC1110_RADIO_DATA_RATE: flag = Radio_Datarate(cmd);
            break;
        case CC1110_RADIO_MODULATION: flag = Radio_Modulation(cmd);
            break;
        case CC1110_RADIO_CARRIER_FREQUENCY: flag = Radio_Carrier_Freq(cmd);
            break;
        case CC1110_CMD_CHANGESTATE: flag = Radio_Change_State(cmd);
            break;
        case CC1110_RETURNCOUNTPACK: flag = Assert_cnt_Pack(cmd);
            break;
        case CC1110_CLEAR_TEMP_RF_REGISTER: flag = Clear_RF_Reg();
            break;

        default: flag = FALSE;
            break;
    }

    if(flag == CNT_PKT)
    {
        countTX_Buffer+=1;
        *(cmd+1) = countTX_Buffer;
        *(cmd + (countTX_Buffer-2)) = SUCCESS;
        *(cmd + (countTX_Buffer-1)) = Cal_CRC(cmd, countTX_Buffer);

        Resp_Package(cmd, *(cmd+1));

        if(Return_Flag)
        {
            Return_Flag = RESET;
        }

    }
    else
    {
        countTX_Buffer += 1;
        *(cmd+1) = countTX_Buffer;
        *(cmd+(countTX_Buffer-2)) = flag;
        *(cmd+(countTX_Buffer-1)) = Cal_CRC(cmd, countTX_Buffer);

        Resp_Package(cmd, *(cmd+1));
    }
}


/******************************************************************************
* @fn LED_Command (Command about LED)
*
* @brief This function executes command which will drive LED.
*
* @param led_cmd: Unsigned char pointer point to array which store LED command.
*
*/

uint8 LED_Command(uint8 *led_cmd)
{
    /********************************************************
    * LED has 2 color viz GREEN and RED, control by PORT 1
    * P1_0 = P1.0 connected with Green LED.
    * P1_1 = P1.1 connected with Red LED.
    * Both 2 color of LED drive with logic high.
    */
   switch(*(led_cmd+3))
   {
       case CC1110_LED_GREEN_ON   : P1_0 = 1;  break;
       case CC1110_LED_GREEN_OFF  : P1_0 = 0;  break;
       case CC1110_LED_RED_ON     : P1_1 = 1;  break;
       case CC1110_LED_RED_OFF    : P1_1 = 0;  break;
       case CC1110_LED_ALL_TOGGLE : P1 = P1^0x03; break;
       default : return FALSE;
   }
    return SUCCESS;
}


/******************************************************************************
* @fn Clear_RF_Reg
*
* @brief This function will clear temp variables of RF registers. These variables
*        will store value that will be written to RF registers.
*
* @param void
*
* @return SUCCESS
*
* NOTE : This function is 1st command from Tester when initial module before testing.
*/

uint8 Clear_RF_Reg(void)
{
    temp_mdmcfg2    = 0;
    temp_mdmcfg1    = 0;
    temp_pktctrl0   = 0;
    temp_frend0     = 0;
    temp_mdmcfg4    = 0;
    temp_mdmcfg3    = 0;
    temp_fscal3     = 0;
    temp_freq2      = 0;
    temp_freq1      = 0;
    temp_freq0      = 0;


    return SUCCESS;
}



/******************************************************************************
* @fn Radio_Package_Format
*
* @brief This function will set temp of variables viz mdmcfg2, mdmcfg1 and
*        pktctrl0. These variables relate with configure RF package format.
*
* @param PKT: Unsigned char pointer point to array of command.
*
* @return SUCCESS : If receive correct command.
*         FALSE   : If receive incorrect command.
*
* NOTE : This function is 3rd command from Tester when initial module before testing.
*/

uint8 Radio_Package_Format(uint8 *PKT)
{
    switch(*(PKT+3))
    {
    case    NRZ_NO_WHITENING_NO_CRC:        temp_mdmcfg2 |= MDMCFG2_NRZ + DEFAULT_SYNC_MODE;
                                            temp_mdmcfg1 |= DEFAULT_MDMCFG1;
				                            temp_pktctrl0 |= PKTCTRL0_LEN;
                                            break;

    case    NRZ_WHITENING_CRC:              temp_mdmcfg2 |= MDMCFG2_NRZ + DEFAULT_SYNC_MODE;
                                            temp_mdmcfg1 |= DEFAULT_MDMCFG1;
                                            temp_pktctrl0 |= PKTCTRL0_LEN + PKTCTRL0_WHITENING + PKTCTRL0_CRC;
                                            break;

    case    MAN_NO_WHITENING_NO_CRC:        temp_mdmcfg2 |= MDMCFG2_MAN + DEFAULT_SYNC_MODE;
                                            temp_mdmcfg1 |= DEFAULT_MDMCFG1;
                                            temp_pktctrl0 |= PKTCTRL0_LEN;
                                            break;

    case    MAN_NO_WHITENING_CRC:           temp_mdmcfg2 |= MDMCFG2_MAN + DEFAULT_SYNC_MODE;
                                            temp_mdmcfg1 |= DEFAULT_MDMCFG1;
                                            temp_pktctrl0 |= PKTCTRL0_LEN + PKTCTRL0_CRC;
                                            break;

    case    TX_WAKEUP:                      temp_mdmcfg2 |= MDMCFG2_NRZ + NO_SYNC_WORD;
                                            temp_mdmcfg1 |= 0x02;
                                            temp_pktctrl0 |= PKTCTRL0_LEN;
                                            break;

    default: return FALSE;
    }
    return SUCCESS;
}





/******************************************************************************
* @fn Radio_Modulation
*
* @brief This function will set temp of variables viz frend0, mdmcfg2 and
*        mdmcfg4. These variables relate with configure RF modulation.
*
* @param MOD: Unsigned char pointer point to array of command.
*
* @return SUCCESS : If receive correct command.
*         FALSE   : If receive incorrect command.
*
* NOTE : This function is 6th command from Tester when initial module before testing.
*/

uint8 Radio_Modulation(uint8 *MOD)
{
    switch(*(MOD+3))
    {
    case MODULATION_OOK:    temp_frend0 = FREND0_OOK;
                            temp_mdmcfg2 |= MDMCFG2_OOK;
                            switch(temp_mdmcfg4)
                            {
                                case MDMCFG4_10K: temp_mdmcfg4 |= MDMCFG4_BW_10K_OOK;
                                    break;
                                case MDMCFG4_32K: temp_mdmcfg4 |= MDMCFG4_BW_32K_OOK;
                                    break;
                                case MDMCFG4_100K: temp_mdmcfg4 |= MDMCFG4_BW_100K_OOK;
                                    break;
                                case MDMCFG4_200K: temp_mdmcfg4 |= MDMCFG4_BW_200K_OOK;
                                    break;
                                default: break;
                            }
         break;
    /* For this case MODULATION_FSK_433 and MODULATION_FSK_868 is same value */
    case MODULATION_FSK_433:    temp_frend0 = FREND0_FSK;
                                temp_mdmcfg2 |= MDMCFG2_FSK;
                                switch(temp_mdmcfg4)
                                {
                                    case MDMCFG4_10K: temp_mdmcfg4 |= MDMCFG4_BW_10K_FSK;
                                        break;
                                    case MDMCFG4_32K: temp_mdmcfg4 |= MDMCFG4_BW_32K_FSK;
                                        break;
                                    case MDMCFG4_100K: temp_mdmcfg4 |= MDMCFG4_BW_100K_FSK;
                                        break;
                                    case MDMCFG4_200K: temp_mdmcfg4 |= MDMCFG4_BW_200K_FSK;
                                        break;
                                    default: break;
                                }
						        //temp_mdmcfg4 |= MDMCFG4; //force to 868 FSK
         break;

    default: return FALSE;
    }
    return SUCCESS;
}




/******************************************************************************
* @fn Radio_Datarate
*
* @brief This function will set temp of variables viz mdmcfg3, mdmcfg4 and
*        fscal3. These variables relate with configure RF data rate.
*
* @param D_RATE: Unsigned char pointer point to array of command.
*
* @return SUCCESS : If receive correct command.
*         FALSE   : If receive incorrect command.
*
* NOTE : This function is 4th command from Tester when initial module before testing.
*/

uint8 Radio_Datarate(uint8 *D_RATE)
{
    switch(*(D_RATE+3))
    {
    case DATARATE_10K:  temp_mdmcfg3 = MDMCFG3_10K;
                        temp_mdmcfg4 |= MDMCFG4_10K;
                        temp_fscal3 = FSCAL_10K;
        break;
    case DATARATE_32K:  temp_mdmcfg3 = MDMCFG3_32K;
                        temp_mdmcfg4 |= MDMCFG4_32K;
                        temp_fscal3 = FSCAL_32K;
        break;
    case DATARATE_100K:  temp_mdmcfg3 = MDMCFG3_100K;
                         temp_mdmcfg4 |= MDMCFG4_100K;
                         temp_fscal3 = FSCAL_100K;
        break;
    case DATARATE_200K:  temp_mdmcfg3 = MDMCFG3_200K;
                         temp_mdmcfg4 |= MDMCFG4_200K;
                         temp_fscal3 = FSCAL_200K;
        break;

    default: return FALSE;
    }
    return SUCCESS;
}



/******************************************************************************
* @fn Radio_Init
*
* @brief This function will set RF registers which not be configured from another
*        functions and these registers are constant values.
*
* @param void
*
* @return SUCCESS only.
*/

uint8 Radio_Init(void)
{
    SYNC1        = DEFAULT_SYNC1;
    SYNC0        = DEFAULT_SYNC0;
    PKTLEN       = DEFAULT_PKTLEN;
    PKTCTRL1     = 0x21;
    ADDR         = 0x1D;
    CHANNR       = 0x00;
    FSCTRL1      = 0x06;
    FSCTRL0      = 0x00;
    DEVIATN      = 0x50;
    MCSM2        = 0x07;
    MCSM1        = 0x30;
    MCSM0        = 0x18;
    FOCCFG       = 0x16;
    BSCFG        = 0x6D;
    AGCCTRL2     = 0x03;
    AGCCTRL1     = 0x40;
    AGCCTRL0     = 0x91;
    FREND1       = 0x56;
    FSCAL2       = 0x2A;
    FSCAL1       = 0x00;
    FSCAL0       = 0x1F;

    PA_TABLE7    = 0x00;
    PA_TABLE6    = 0x00;
    PA_TABLE5    = 0x00;
    PA_TABLE4    = 0x00;
    PA_TABLE3    = 0x00;
    PA_TABLE2    = 0x00;
    PA_TABLE1    = 0xC0;
    PA_TABLE0    = 0x00;

    return SUCCESS;
}



/******************************************************************************
* @fn Radio_Carrier_Freq (Radio Carrier Frequency)
*
* @brief This function will set temp of variables viz freq2, freq1 and
*        freq0. These variables relate with configure RF carrier frequency.
*
* @param freq: Unsigned char pointer point to array of command.
*
* @return SUCCESS only.
*
* NOTE : This function is 2nd and 5th command from Tester when initial module before testing.
*/

uint8 Radio_Carrier_Freq(uint8 *freq)
{
    temp_freq2        = *(freq + 5);
    temp_freq1        = *(freq + 4);
    temp_freq0        = *(freq + 3);
    return SUCCESS;
}



/******************************************************************************
* @fn dmaRadioConfig
*
* @brief This function will configure DMA for receive or transmit RF data. DMA
*        is referred from data sheet of CC1110 in "DMA Configuration Data Structure".
*        1. PRIORITY : The DMA channel priority
*        2. M8       : Mode of 8th bit for VLEN transfer length ; only applicable when WORDSIZE = 0.
*        3. IRQMASK  : Interrupt Mask for this channel.
*        4. TRIG     : Select DMA trigger to use.
*        5. TMODE    : The DMA channel transfer mode.
*        6. WORDAIZE : Selects whether each DMA transfer shall be 8bits(0) or 16bits(1).
*        7. SRCADDRH  : The DMA channel source address high.
*           SRCADDRL  : The DMA channel source address low.
*        8. DESTADDRH : The DMA channel destination address high.
*           DESTADDRL : The DMA channel destination address low.
*           *NOTE     : The flash memory is not directly writable.*
*        9. VLEN      : Variable length transfer mode (Consider bits 12:0 of the first word).
*        10.LENH      : Bits 12:8 of the first word.
*        11.LENL      : Bits 7:0 of the first word.
*        12.SRCINC    : Source address increment mode (after each transfer).
*        13.DESTINC   : Destination address increment mode (after each transfer).
*        14.DMAxCFGH  : DMA Channel x Configurable Address High Byte.
*        15.DMAxCFGH  : DMA Channel x Configurable Address Low Byte.
*
* @param radioMODE : Indicate mode and DMA registers configuration for RF.
*
* @return void.
*/

void dmaRadioConfig(uint8 radioMODE)
{
  /* DMA Setup
     Some configuration that are common for both TX and RX:

   * CPU has priority over DMA
   * Use 8 bits for transfer count
   * No DMA interrupt when done
   * DMA triggers on radio
   * Single transfer per trigger.
   * One byte is transferred each time.
   */
  dmaConfig0.PRIORITY       = DMA_PRI_LOW;
  dmaConfig0.M8             = DMA_M8_USE_8_BITS;
  dmaConfig0.IRQMASK        = DMA_IRQMASK_DISABLE;
  dmaConfig0.TRIG           = DMA_TRIG_RADIO;
  dmaConfig0.TMODE          = DMA_TMODE_SINGLE;
  dmaConfig0.WORDSIZE       = DMA_WORDSIZE_BYTE;

  // Receiver specific DMA settings:

  // Source: RFD register
  // Destination: radioPktReceice
  // Use the first byte read + 3 (incl. 2 status bytes)
  // Sets maximum transfer count allowed (length byte + data + 2 status bytes)
  // Data source address is constant
  // Destination address is incremented by 1 byte for each write

  if(radioMODE == _TX_)
  {
    // Transmitter specific DMA settings

    // Source: radioPktBuffer
    // Destination: RFD register
    // Use the first byte read + 1
    // Sets the maximum transfer count allowed (length byte + data)
    // Data source address is incremented by 1 byte
    // Destination address is constant
    dmaConfig0.SRCADDRH   = ((uint16)(radio_Packet_TX) >> 8) & 0x00FF;
    dmaConfig0.SRCADDRL   = (uint16)(radio_Packet_TX) & 0x00FF;
    dmaConfig0.DESTADDRH  = ((uint16)&X_RFD >> 8) & 0x00FF;
    dmaConfig0.DESTADDRL  = (uint16)&X_RFD & 0x00FF;

    dmaConfig0.VLEN       = DMA_VLEN_1_P_VALOFFIRST;

    dmaConfig0.LENH       = (((uint16)Buffer_Length + 1) >> 8) & 0x00FF;
    dmaConfig0.LENL       = ((uint16)Buffer_Length + 1 ) & 0x00FF;
    dmaConfig0.SRCINC     = DMA_SRCINC_1;
    dmaConfig0.DESTINC    = DMA_DESTINC_0;

  }

  else if(radioMODE == _RX_)
  {
    // Receiver specific DMA settings:

    // Source: RFD register
    // Destination: radioPktReceice
    // Use the first byte read + 3 (incl. 2 status bytes)
    // Sets maximum transfer count allowed (length byte + data + 2 status bytes)
    // Data source address is constant
    // Destination address is incremented by 1 byte for each write
    dmaConfig0.SRCADDRH   = ((uint16)&X_RFD >> 8) & 0x00FF;
    dmaConfig0.SRCADDRL   = (uint16)&X_RFD & 0x00FF;
    dmaConfig0.DESTADDRH  = ((uint16)radio_Packet_RX >> 8) & 0x00FF;
    dmaConfig0.DESTADDRL  = (uint16)radio_Packet_RX & 0x00FF;

    dmaConfig0.VLEN       = DMA_VLEN_1_P_VALOFFIRST;

    dmaConfig0.LENH       = (((uint16)Buffer_Length + 1) >> 8) & 0x00FF;
    dmaConfig0.LENL       = ((uint16)Buffer_Length + 1) & 0x00FF;
    dmaConfig0.SRCINC     = DMA_SRCINC_0;
    dmaConfig0.DESTINC    = DMA_DESTINC_1;

  }

  // Save pointer to the DMA configuration struct into DMA-channel 0
  // configuration registers
  DMA0CFGH = ((uint16)&dmaConfig0 >> 8) & 0x00FF;
  DMA0CFGL = (uint16)&dmaConfig0 & 0x00FF;

__asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP");
__asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP");
__asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP");
__asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP");
__asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP");
__asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP"); __asm__ ("NOP");



}






/******************************************************************************
* @fn Radio_Change_State
*
* @brief   This function will set RF state register and enable RF interrupt.
*
* @param    state : Unsigned char pointer points to array of command.
*
* @return   SUCCESS : Correct Command.
*           FALSE   : Incorrect Command.
*
* NOTE : This function is 7th command from Tester when initial module before testing.
*/

uint8 Radio_Change_State(uint8 *state)
{
    uint8 i;
    switch(*(state+3))
    {
    case    STATE_IDLE:     RF_State = _IDLE_;
                            /* Enable RF interrupt mask IRQ done (Transmit/Receive completed) */
                            IEN2 |= IEN2_RFIE;     // Enable RF general interrupt
                            RFIM  = RFIM_IM_DONE;  // Mask IRQ_DONE flag only
                            RFST = RFST_SIDLE;
                            while ((MARCSTATE & MARCSTATE_MARC_STATE) != MARC_STATE_IDLE);

                            break;
    case    STATE_RX:       RF_State = _RX_;

                             /************************* Enable RX state ********************************/
                              IEN2 |= IEN2_RFIE;     // Enable RF general interrupt
                              RFIM  = RFIM_IM_DONE;  // Mask IRQ_DONE flag only
                              RFST = RFST_SIDLE;
                              while ((MARCSTATE & MARCSTATE_MARC_STATE) != MARC_STATE_IDLE);

                              radio_setting_RX();
                              DMAARM = 0x80 | DMAARM0; // abort any DMA transfer that might be in progress
                              dmaRadioConfig(RF_State);  //Configure DMA for Receive data.
                              DMAARM |= DMAARM0 ;// arm the dma channel for receive
                              RFST = RFST_SRX;   // send strobe to enter receive mode
                            /***************************************************************************/

                            break;
    case    STATE_TX:       RF_State = _TX_;
                            radio_setting_TX();
                            dmaRadioConfig( _TX_ );    /* configure DMA channel for receive */
                            /*************** Set packet for transmit *******************/
                            for(i=0; i<22; i++)
                            {
                                radio_Packet_TX[i] = radioBuffer_RX_Test[i];
                            }
                            /***********************************************************/
                            DMAARM = 0x80 | DMAARM0;/* abort any DMA transfer that might be in progress */
                            DMAARM |= DMAARM0 ; /* arm the dma channel for transmit */
                            RFST =  RFST_STX;/* send strobe to enter transmit mode */
                            break;

        default: return FALSE;
    }

    return SUCCESS;
}

/*******************************************************************************************************
* @fn Assert_cnt_Pack
*
* @brief This function uses insert count packages both ok and error packages in to response package.
*
* @param cnt_pack : unsigned char pointer points to array of command.
*
* @return CNT_PKT.
*/

uint8 Assert_cnt_Pack(uint8 *cnt_pack)
{
    if((pkt_error_cnt + pkt_ok_cnt) == 10)
    {

    }
    else
    {
        if((pkt_error_cnt <= 10)&&(pkt_ok_cnt <= 10))
        {
            if(pkt_ok_cnt == 0)
            {
                pkt_error_cnt = 10;
            }
            else if(pkt_ok_cnt != 0)
            {
                pkt_error_cnt = 10 - pkt_ok_cnt;
            }
        }
//        else
//        {
//                pkt_error_cnt = 0;
//                pkt_ok_cnt = 0;
//        }
    }

    countTX_Buffer += 2;
    *(cnt_pack + 3) = pkt_ok_cnt;
    *(cnt_pack + 4) = pkt_error_cnt;
    pkt_error_cnt = 0;
    pkt_ok_cnt = 0;

    return CNT_PKT;
}



/*******************************************************************************************************
* @fn radio_setting_TX
*
* @brief This function configures transmission RF registers
*
* @param  void
*
* @return void
*/

void radio_setting_TX(void)
{
   /*This configure use for TX as same as Tester board*/

    SYNC1       = 0x4C;  //           Sync Word, High Byte
    SYNC0       = 0xB2;  //           Sync Word, Low Byte
    PKTLEN      = 0xF0;
    PKTCTRL1    = 0x04;
    PKTCTRL0    = 0x45;  //        Packet Automation Control, Whitening data , Calculation CRC
    ADDR        = 0x1D;
    //CHANNR      = 0x00;
    FSCTRL1     = 0x0F;  //        Frequency Synthesizer Control
    //FSCTRL0     = 0x00;

    FREQ2       = 0x10;  //           Frequency Control Word, High Byte
    FREQ1       = 0xB0;  //           Frequency Control Word, Middle Byte
    FREQ0       = 0x71;  //           Frequency Control Word, Low Byte
    MDMCFG4     = 0x0A | 0x20;  //        Modem configuration
    MDMCFG3     = 0x4A; //         Modem Configuration
    MDMCFG2     = 0x30 | (MDMCFG2_NRZ + DEFAULT_SYNC_MODE);  //         Modem Configuration
    MDMCFG1     = DEFAULT_MDMCFG1;
    //MDMCFG0     = 0xF8;

    DEVIATN     = 0x50;  // DEVIATN        Modem Deviation Setting
    //MCSM2       = 0x07;
    MCSM1       = 0x30;
    MCSM0       = 0x18;  // MCSM0          Main Radio Control State Machine Configuration

    FOCCFG      = 0x17;  // FOCCFG         Frequency Offset Compensation Configuration
    BSCFG       = 0x6C;

    AGCCTRL2     = 0x03;
    AGCCTRL1     = 0x40;
    AGCCTRL0     = 0x91;

    FREND1      = 0x56;
    FREND0      = 0x11;  // FREND0         Front End TX Configuration
    FSCAL3      = 0xE9;  // FSCAL3         Frequency Synthesizer Calibration


    FSCAL2       = 0x2A;
    FSCAL1       = 0x00;
    FSCAL0       = 0x1F;
    TEST2        = 0x88;
    TEST1        = 0x31;
    TEST0        = 0x09;

//    PA_TABLE7    = 0x00;
//    PA_TABLE6    = 0x00;
//    PA_TABLE5    = 0x00;
//    PA_TABLE4    = 0x00;
//    PA_TABLE3    = 0x00;
//    PA_TABLE2    = 0x00;
    PA_TABLE1    = 0x12;  // PA_TABLE1      PA Power Setting 1
    PA_TABLE0    = 0x00;

    IOCFG0      = 0x06;
//    LQI         = 0x98;
//    PKTSTATUS   = 0x80;

}



/*******************************************************************************************************
* @fn radio_setting_RX
*
* @brief This function configures receive RF registers
*
* @param  void
*
* @return void
*/

void radio_setting_RX(void)
{
    SYNC1        = DEFAULT_SYNC1;
    SYNC0        = DEFAULT_SYNC0;
    PKTLEN       = DEFAULT_PKTLEN;
    PKTCTRL1     = 0x21;
    PKTCTRL0     = temp_pktctrl0;
    ADDR         = 0x1D;// Texus 0x00
    CHANNR       = 0x00;
    FSCTRL1      = 0x06;// 0x06
    FSCTRL0      = 0x00;
    FREQ2        = temp_freq2;
    FREQ1        = temp_freq1;
    FREQ0        = temp_freq0;
    MDMCFG4      = temp_mdmcfg4;
    MDMCFG3      = temp_mdmcfg3;
    MDMCFG2      = temp_mdmcfg2;
    MDMCFG1      = temp_mdmcfg1;
    DEVIATN      = 0x50;// freq. deviation is 50.78125 kHz
    MCSM2        = 0x07;
    MCSM1        = 0x30;
    MCSM0        = 0x18;
    FOCCFG       = 0x16;
    BSCFG        = 0x6D;
    AGCCTRL2     = 0x03;
    AGCCTRL1     = 0x40;
    AGCCTRL0     = 0x91;
    FREND1       = 0x56;
    FREND0       = temp_frend0;
    FSCAL3       = temp_fscal3;
    FSCAL2       = 0x2A;
    FSCAL1       = 0x00;
    FSCAL0       = 0x1F;
//    TEST2        = 0x88;
//    TEST1        = 0x31;
//    TEST0        = 0x09;

    PA_TABLE7    = 0x00;
    PA_TABLE6    = 0x00;
    PA_TABLE5    = 0x00;
    PA_TABLE4    = 0x00;
    PA_TABLE3    = 0x00;
    PA_TABLE2    = 0x00;
    PA_TABLE1    = 0xC0;
    PA_TABLE0    = 0x00;

//    IOCFG0      = 0x06;
//    LQI         = 0x98;
//    PKTSTATUS   = 0x80;

}


