
/***********************************************************************************
  Filename:     main.c

  Description:  This file shows user about flow of firmware of CC1110 module. Purpose
                is sensitivity test of CC1110 when send packages by using RF
                follow these conditions :
                  1. Data rate of RF communication viz 10kbps, 32kbps, 100kbps and 200kbps.
                  2. Carrier frequency 433.92 MHz.
                  3. Modulation OOK/ASK.
                  4. Data coding viz NRZ and Manchester
                  5. Various Transmit Power.

  Version:      V1.00

  Modified by :    Worrakamol B. (ES - APP team)

***********************************************************************************/



/***********************************************************************************
* INCLUDES
*/
#include "CC1110_test_module.h"
#include "uart-if.h"


/***********************************************************************************
* CONSTANTS
*/

/*************** About Packet Transmit **************************
* radioBuffer_RX_Test[0] : Length Bytes TX
* radioBuffer_RX_Test[1] : Device ADDR
* radioBuffer_RX_Test[2] : Header
* radioBuffer_RX_Test[3 until lastbyte - 1] : Data
* radioBuffer_RX_Test[last byte] : CRC is got from radioBuffer_RX_Test[1] xor with every data until radioBuffer_RX_Test[last byte - 1]
*/
//{20,0x1d,0x0d,0x00,0x12,0x5f,0x15,0xfc,0x65,0xe2,0xeb,0x5d,0xdc,0xb0,0x23,0x9e,0x59,0x8c,0x27,0x59,0x69,0x96};
//uint8 radioBuffer_RX_Test[22] = {21,0x1d,0x0d,0x00,0x12,0x5f,0x15,0xfc,0x65,0xe2,0xeb,0x5d,0xdc,0xb0,0x23,0x9e,0x59,0x8c,0x27,0x59,0x69,0x96}; // Texus
uint8 radioBuffer_RX_Test[22] = {20,0x1d,0x0d,0x00,0x12,0x5f,0x15,0xfc,0x65,0xe2,0xeb,0x5d,0xdc,0xb0,0x23,0x9e,0x59,0x8c,0x27,0x59,0x69,0x96}; // Tester
/***********************************************************************************
* LOCAL VARIABLES
*/
volatile uint8 cmd_buffer[Buffer_Length]={0}; // Command Buffer is used to store command from Tester.
volatile uint8 radio_Packet_RX[Buffer_Length] = {0};
volatile uint8 radio_Packet_TX[Buffer_Length] = {0};
volatile uint8 pkt_error_cnt = 0;
volatile uint8 pkt_ok_cnt = 0;
volatile uint8 ReceiveFlag = RESET;
volatile uint8 Return_Flag = RESET;
/***********************************************************************************
* LOCAL FUNCTIONS
*/
void Debug_LED(void);
void System_Config(void);
void init_LED(void);



/***********************************************************************************
* @fn          main
*
* @brief       Describe what the firmware is doing.
*
* @param       none
*
* @return      0
*/

int main(void) {
    uint8 flag = FALSE;

    /* Init System Clock Source */
    System_Config();

    //Init port 1 which use pin for drive LED
    init_LED();

    /* Init UART0 for get data from tester board */
    init_uart();

    //radio_setting_debug_RX();

    IEN2 |= IEN2_RFIE;     // Enable RF general interrupt
    RFIM  = RFIM_IM_DONE;  // Mask IRQ_DONE flag only
    RFST = RFST_SIDLE;      // Change state RF to IDLE state.
    while ((MARCSTATE & MARCSTATE_MARC_STATE) != MARC_STATE_IDLE);// While Loop wait for RF entry to

    /* Start listening U0_RX */
    Start_Listening_Packages();


    /* Toggle LED */
        P1_1 = 1; // Red on
        P1_0 = 1; // Green on
        halWait(50);
        P1_1 = 0;
        P1_0 = 0;
        halWait(50);

      /* Enable global interrupt by setting the [IEN0.EA=1] */
      EA = 1;

    while(1)
    {

        while(!flag)
        {
          flag = Get_Protocol_S(cmd_buffer);
        }
        flag = FALSE;
        Select_cmd(cmd_buffer);

    }


}


/************************************************************************
 Function in this file =================================================*/




/*******************************************************************************************************
* @fn System_Config
*
* @brief This function initials system clock.
*
* @param  void
*
* @return void
*/

void System_Config(void)
{
  /* Set the system clock source to HS XOSC and max CPU speed,
  * ref. [clk]=>[clk_xosc.c]
  */
  SLEEP &= ~SLEEP_OSC_PD;			//XOSC and HS RCOSC power down setting :0 � Both oscillators powered up , 1 � Oscillator not selected by CLKCON.OSC bit powered down
  while( !(SLEEP & SLEEP_XOSC_S) ); //Wait for XOSC_STB = 1 : 26 MHz Crystal oscillator stable status = powered up and stable
  CLKCON = (CLKCON & ~(CLKCON_CLKSPD | CLKCON_OSC | CLKCON_OSC32)) | CLKSPD_DIV_1;  //Store value of TICKSPD bits and force another bits to 0.
  CLKCON = 0x00; //Store 0x00 in CLKCON.
  while (CLKCON & CLKCON_OSC);
  SLEEP |= SLEEP_OSC_PD;  // Set bit 2 of SLEEP : 1 � Oscillator not selected by CLKCON.OSC bit powered down

}





/*******************************************************************************************************
* @fn init_LED
*
* @brief This function use initialize LED Green and Red
*
* @param  void
*
* @return void
*/

void init_LED(void)
{
  /*
  * P1_0  LED Green
  * P1_1  LED Red
  */
    /* Port1 Function Select */
    P1SEL &= ~(BIT1 | BIT0);
    /* Change direction to output */
    P1DIR |= BIT1 | BIT0;

}




/************************************************************************
 INTERRUPT SERVICE ROUTINES ============================================*/

/*******************************************************************************************************
* @fn Interrupt UART0 Receive
*
* @brief This function is ISR that will receive data from UART0.
*
* @param  void
*
* @return void
*/

void UART0_RX_ISR(void) __interrupt(URX0_VECTOR)
{
  URX0IF = 0;
  RX_Buffer[countRX_Buffer++] = U0DBUF;
  if((RX_Buffer[countRX_Buffer-1] == 0xB8)&&(RF_State == _RX_))
  {
      RF_State = _CHANGE_;
  }
  else if((RX_Buffer[countRX_Buffer-1] == 0xBB)&&(RF_State == _RX_))
  {
      Return_Flag = SET;
  }
}


/*******************************************************************************************************
* @fn Interrupt UART0 Transmit.
*
* @brief This function is ISR that will transmit data by UART0.
*
* @param  void
*
* @return void
*/

void UART0_TX_ISR(void) __interrupt(UTX0_VECTOR)
{
  UTX0IF = 0;   //Clear any pending UART TX Interrupt Flag
  //U0CSR &= ~U0CSR_TX_BYTE;   // UxCSR.TX_BYTE = 0 : Byte not transmitted
  U0DBUF = Tx_Buffer[countTX_Buffer++]; // Send very first UART byte
  //IEN2 |= IEN2_UTX0IE;  // UART TX Interrupt (IEN2.UTXxIE = 1)
  //if((countData == 7)||(countData == 5)||(countData == 19))
  if(countTX_Buffer == Tx_length)
  {
      uint8 i;
      IEN2 &= ~IEN2_UTX0IE; //disable U0TX interrupt
      countTX_Buffer = 0;
      for(i=0; i<Buffer_Length; i++){Tx_Buffer[i]=0;} //Clear data buffer to send
  }
}



/*******************************************************************************************************
* @fn Interrupt RF TX/RX.
*
* @brief The only interrupt flag which throws this interrupt is the IRQ_DONE interrupt.
*        So this is the code which runs after a packet has been received or
*        transmitted.
*
* @param  void
*
* @return void
*/

void RF_IRQ(void) __interrupt(RF_VECTOR)
{
    //uint8 data_OK = RFIF;

    if( RFIF & RFIF_IRQ_DONE )
    {
        if(RF_State == _RX_)
            {
                uint8 cnt;
                uint8 data_ok = 1;
                /* Green LED toggle */
                //ReceiveFlag = SET;

                /************************* Check Package ********************************/
                    for(cnt=0; cnt<radio_Packet_RX[0]; cnt++)
                    {
                        if(radio_Packet_RX[cnt] != radioBuffer_RX_Test[cnt])
                        {
                            data_ok = 0;
                            break; //break for loop
                        }
                        else
                        {
                            continue;
                        }
                    }

                    if(data_ok)
                    {
                        P1_0 = 1;
                        halWait(30);
                        P1_0 = 0;
                        pkt_ok_cnt++;
                        //break; //break while loop
                    }
                    else
                    {
                        P1_1 = 1;
                        halWait(30);
                        P1_1 = 0;
                        pkt_error_cnt++;
                    }
                /*************************************************************************/


                /************************** Clear Flag ***********************************/

                    /* Clear RFIF_IRQ_DONE flag */
                    //RFIF &= ~RFIF_IRQ_DONE;
                    RFIF = 0x00;

                  /* Clear the general RFIF interrupt registers */
                    S1CON &= ~0x03;
                /**************************************************************************/

                /************************* Enable RX state ********************************/
                   IEN2 |= IEN2_RFIE;     // Enable RF general interrupt
                   RFIM  = RFIM_IM_DONE;  // Mask IRQ_DONE flag only
                   RFST = RFST_SIDLE;
                   while ((MARCSTATE & MARCSTATE_MARC_STATE) != MARC_STATE_IDLE);

                   radio_setting_RX();
                   DMAARM = 0x80 | DMAARM0; // abort any DMA transfer that might be in progress
                   dmaRadioConfig(RF_State);  //Configure DMA for Receive data.
                   DMAARM |= DMAARM0 ;// arm the dma channel for receive
                   RFST = RFST_SRX;   // send strobe to enter receive mode
               /***************************************************************************/
            }
        else if(RF_State == _TX_)
            {
                /* Green LED toggle */
                P1_0 = 1;
                halWait(80);
                P1_0 = 0;
                halWait(80);
                P1_0 = 1;
                halWait(80);
                P1_0 = 0;

                      /* Clear RFIF_IRQ_DONE flag */
                //RFIF &= ~RFIF_IRQ_DONE;
                RFIF = 0x00;

              /* Clear the general RFIF interrupt registers */
                S1CON &= ~0x03;

            }

    }

}







